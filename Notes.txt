Changes:
	

ToDo:
	Add zoom in/out
	
	Add resizing
		Change mouse to the appropriate resize icon when mousing over edges of Class when Resize is enabled
	
	Add undo/redo
		Use a stack implemented as a linked list
	
	Shifting variable and method objects up/down

Final Benchmark:
	[DONE] Integrated file management
		[DONE] export to photo
		[DONE] Fix import statements in export to code
	[DONE] Adding interfaces
	[DONE] Adding/editing/removing
		[DONE] Parents (drop down box)
		[DONE] Interfaces (drop down box with checkmarks)
		[DONE] Variables
		[DONE] Methods
	[DONE] Display variables and methods in UML boxes
	Resize class/interface
	[DONE] Remove class/interface
	Undo/redo
	Zoom in/out
	[DONE] Toggle grid rendering
	[DONE] Toggle grid snapping
	[DONE] Connector line rendering
		[DONE] Moving a class should move the line as well
	Connector line editing

Bugs:
	Fix code exporting not sending to correct directory