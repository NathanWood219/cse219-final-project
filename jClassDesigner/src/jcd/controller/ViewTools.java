package jcd.controller;

import jcd.data.DataManager;
import saf.AppTemplate;

/**
 *
 * @author Nathan
 */
public class ViewTools {
    AppTemplate app;
    DataManager dataManager;
    
    public ViewTools(AppTemplate initApp) {
	app = initApp;
	dataManager = (DataManager)app.getDataComponent();
    }
    
    public void handleZoomIn() {
        dataManager.zoomIn();
    }
    
    public void handleZoomOut() {
        dataManager.zoomOut();
    }
    
    public void handleToggleRendering(boolean checkBoxValue) {
        dataManager.setGridRender(checkBoxValue);
    }
    
    public void handleToggleSnapping(boolean checkBoxValue) {
        dataManager.setGridSnap(checkBoxValue);
    }
}
