package jcd.controller;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static jcd.PropertyType.COMPONENT_ADD_ICON;
import static jcd.PropertyType.COMPONENT_ADD_TOOLTIP;
import static jcd.PropertyType.COMPONENT_REMOVE_ICON;
import static jcd.PropertyType.COMPONENT_REMOVE_TOOLTIP;
import jcd.data.DataManager;
import jcd.objects.MethodObj;
import saf.AppTemplate;
import jcd.objects.VariableObj;
import saf.ui.AppGUI;

/**
 *
 * @author Nathan
 */
public class ComponentTools {
    AppTemplate app;
    DataManager dataManager;
    AppGUI gui;
    
    // THIS IS USED FOR POPUP WINDOWS
    Stage stage;
    
    // ADDING A NEW CLASS VS EDITING A CLASS
    boolean isNew = false;
    
    // ARGS FOR A NEW/EDITED METHOD
    VBox argsBox;
    ArrayList<VariableObj> args;
    
    public ComponentTools(AppTemplate initApp) {
	app = initApp;
	dataManager = (DataManager)app.getDataComponent();
        gui = app.getGUI();
        
        args = new ArrayList();
        
        argsBox = new VBox(10);
        argsBox.setPadding(new Insets(10));
    }
    
    public void handleEditName(String name) {
        dataManager.editName(name);
    }
    
    public void handleEditPackage(String pack) {
        dataManager.editPackage(pack);
    }
    
    public void handleAddVariable() {
        isNew = true;
        handleEditVariable(new VariableObj());
    }
    
    public void handleEditVariable(VariableObj var) {
        stage = new Stage();
        stage.setTitle("Edit Variable");
                
        VBox container = new VBox();
        
        TextField nameField = new TextField(var.getName());
        TextField typeField = new TextField(var.getType());
        
        CheckBox staticCheck = new CheckBox();
        staticCheck.setSelected(var.isStatic());
        
        ComboBox<String> accessCombo = new ComboBox();
        
        accessCombo.getItems().addAll("<default>", "public", "protected", "private");
        accessCombo.setValue(var.getAccess());
        
        HBox nameBox = buildRow("Name:\t\t");
        HBox typeBox = buildRow("Type:\t\t");
        HBox staticBox = buildRow("Is this static?\t");
        HBox accessBox = buildRow("Access:\t\t");
        HBox buttonsBox = new HBox(10);
        
        nameBox.getChildren().add(nameField);
        typeBox.getChildren().add(typeField);
        staticBox.getChildren().add(staticCheck);
        accessBox.getChildren().add(accessCombo);
        
        String confirmText = (isNew ? "Add Variable" : "Edit Variable");
        
        Button confirmButton = new Button(confirmText);
        Button cancelButton = new Button("Cancel");
        
        buttonsBox.setPadding(new Insets(10));
        buttonsBox.setAlignment(Pos.CENTER);
        buttonsBox.getChildren().addAll(confirmButton, cancelButton);
        
        confirmButton.setOnAction(e -> {
            VariableObj newVar = new VariableObj(nameField.getText(), typeField.getText(), accessCombo.getValue(), staticCheck.isSelected());
            
            if(isNew) {
                dataManager.addVariable(newVar);
            } else {
                dataManager.editVariable(var, newVar);
            }
            
            dataManager.update();
            
            isNew = false;
            stage.close();
        });
        
        cancelButton.setOnAction(e -> {
            stage.close();
        });
        
        container.getChildren().addAll(nameBox, typeBox, staticBox, accessBox, buttonsBox);
                
        Scene scene = new Scene(container, 290, 220);
        stage.setScene(scene);
        
        stage.setResizable(false);
        stage.showAndWait();
    }
    
    public void handleRemoveVariable(VariableObj var) {
        dataManager.getSelected().removeVariable(var);
        dataManager.update();
    }
    
    public void handleAddMethod() {
        isNew = true;
        handleEditMethod(new MethodObj());
    }
    
    public void handleEditMethod(MethodObj meth) {
        args = obsListToArrayList(meth.getArgs());
        
        stage = new Stage();
        stage.setTitle("Edit Method");
                
        VBox container = new VBox();
        
        TextField nameField = new TextField(meth.getName());
        TextField returnField = new TextField(meth.getReturnType());
        
        CheckBox staticCheck = new CheckBox();
        staticCheck.setSelected(meth.isStatic());
        
        CheckBox abstractCheck = new CheckBox();
        abstractCheck.setSelected(meth.isAbstract());
        
        ComboBox<String> accessCombo = new ComboBox();
        
        accessCombo.getItems().addAll("<default>", "public", "protected", "private");
        accessCombo.setValue(meth.getAccess());
        
        HBox nameBox = buildRow("Name:\t\t");
        HBox typeBox = buildRow("Return Type:\t");
        HBox staticBox = buildRow("Is static?\t\t");
        HBox abstractBox = buildRow("Is abstract?\t");
        HBox accessBox = buildRow("Access:\t\t");
        HBox buttonsBox = new HBox(10);
        
        nameBox.getChildren().add(nameField);
        typeBox.getChildren().add(returnField);
        staticBox.getChildren().add(staticCheck);
        abstractBox.getChildren().add(abstractCheck);
        accessBox.getChildren().add(accessCombo);
        
        // BUILD THE TABLEVIEW FOR ARGS
        VBox argsVBox = new VBox(10);
        argsVBox.setPadding(new Insets(10));
        HBox argsToolbar = new HBox(10);
        argsToolbar.setAlignment(Pos.CENTER_LEFT);
        
        Label argsLabel = new Label("Arguments:");
        argsToolbar.getChildren().add(argsLabel);
        
        Button argsAdd = gui.initChildButton(argsToolbar, COMPONENT_ADD_ICON.toString(), COMPONENT_ADD_TOOLTIP.toString(), false);

        updateArgs();
        ScrollPane argsScrollPane = new ScrollPane();
        argsScrollPane.setMinHeight(86);
        argsScrollPane.setMaxHeight(86);
        argsScrollPane.setContent(argsBox);
        
        argsVBox.getChildren().addAll(argsToolbar, argsScrollPane);
        
        argsAdd.setOnAction(e -> {
            args.add(new VariableObj("name", "type"));
            updateArgs();
        });
        
        String confirmText = (isNew ? "Add Method" : "Edit Method");
        
        Button confirmButton = new Button(confirmText);
        Button cancelButton = new Button("Cancel");
        
        buttonsBox.setPadding(new Insets(10));
        buttonsBox.setAlignment(Pos.CENTER);
        buttonsBox.getChildren().addAll(confirmButton, cancelButton);
        
        confirmButton.setOnAction(e -> {
            MethodObj newMeth = new MethodObj(nameField.getText(), returnField.getText(),
                    accessCombo.getValue(), staticCheck.isSelected(), abstractCheck.isSelected(), FXCollections.observableArrayList(args));
            
            if(isNew) {
                dataManager.addMethod(newMeth);
            } else {
                dataManager.editMethod(meth, newMeth);
            }
            
            dataManager.update();
            
            args = new ArrayList();
            isNew = false;
            stage.close();
        });
        
        cancelButton.setOnAction(e -> {
            args = new ArrayList();
            stage.close();
        });
        
        container.getChildren().addAll(nameBox, typeBox, staticBox, abstractBox, accessBox, argsVBox, buttonsBox);
                
        Scene scene = new Scene(container, 290, 400);
        stage.setScene(scene);
        
        stage.setResizable(false);
        stage.showAndWait();
    }
    
    private ArrayList<VariableObj> obsListToArrayList(ObservableList<VariableObj> list) {
        ArrayList<VariableObj> arrayList = new ArrayList();
        
        for(VariableObj var : list) {
            arrayList.add(var);
        }
        
        return arrayList;
    }
    
    private void updateArgs() {
        argsBox.getChildren().clear();
        
        for(int i = 0; i < args.size(); i++) {
            final int index = i;
            VariableObj var = args.get(i);
            
            HBox row = new HBox(10);
            row.setAlignment(Pos.CENTER_LEFT);
            
            Label count = new Label("" + (i + 1));
            
            // BUILDING THE NAME FIELD
            TextField nameField = new TextField(var.getName());
            nameField.setMinWidth(80);
            nameField.setMaxWidth(80);
            
            nameField.textProperty().addListener(e -> {
                args.get(index).setName(nameField.getText());
            });
            
            // BUILDING THE TYPE FIELD
            TextField typeField = new TextField(var.getType());
            typeField.setMinWidth(80);
            typeField.setMaxWidth(80);
            
            typeField.textProperty().addListener(e -> {
                args.get(index).setType(typeField.getText());
            });
            
            row.getChildren().addAll(count, nameField, typeField);
            
            Button argRemove = gui.initChildButton(row, COMPONENT_REMOVE_ICON.toString(), COMPONENT_REMOVE_TOOLTIP.toString(), false);
        
            argRemove.setOnAction(e -> {
                args.remove(index);
                updateArgs();
            });
            
            argsBox.getChildren().add(row);
        }
    }
    
    public void handleRemoveMethod(MethodObj meth) {
        dataManager.getSelected().removeMethod(meth);
        dataManager.update();
    }
    
    private HBox buildRow(String text) {
        HBox box = new HBox();
        
        box.setSpacing(40);
        box.setPadding(new Insets(10));
        
        box.getChildren().add(new Label(text));
        
        return box;
    }
}
