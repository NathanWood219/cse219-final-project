package jcd.controller;

import jcd.data.DataManager;
import saf.AppTemplate;
import jcd.objects.ClassObj;

/**
 *
 * @author Nathan
 */
public class EditDesignTools {
    AppTemplate app;
    DataManager dataManager;
    
    public EditDesignTools(AppTemplate initApp) {
	app = initApp;
	dataManager = (DataManager)app.getDataComponent();
    }
    
    public void handleSelect() {
        dataManager.enableSelect();
    }
    
    public void handleResize() {
        dataManager.enableResize();
    }
    
    public void handleAddClass() {
        dataManager.addClass();
    }
    
    public void handleAddInterface() {
        dataManager.addInterface();
    }
    
    public void handleRemove() {
        dataManager.enableDelete();
    }
    
    public void handleUndo() {
        
    }
    
    public void handleRedo() {
        
    }
}
