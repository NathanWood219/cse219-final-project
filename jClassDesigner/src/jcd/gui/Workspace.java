package jcd.gui;

import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import static jcd.PropertyType.*;
import jcd.controller.ComponentTools;
import jcd.controller.EditDesignTools;
import jcd.controller.ViewTools;
import jcd.data.DataManager;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import jcd.objects.MethodObj;
import jcd.objects.VariableObj;
import jcd.objects.ClassObj;
import org.controlsfx.control.CheckComboBox;
import org.controlsfx.control.IndexedCheckModel;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    DataManager dataManager;
    
    // INTERFACE COMPONENTS
    FlowPane topToolbar;
    
    HBox editToolbar;
    HBox viewToolbar;
    
    // EDIT BUTTONS
    Button selectButton;
    Button resizeButton;
    Button addClassButton;
    Button addInterfaceButton;
    Button removeButton;
    Button undoButton;
    Button redoButton;
    
    // VIEW BUTTONS
    Button zoomInButton;
    Button zoomOutButton;
    VBox checkBoxes;
    CheckBox gridCheckBox;
    CheckBox snapCheckBox;
    
    // WORKSPACE
    SplitPane workspaceSplitPane;
    ScrollPane workspaceScrollPane;
    
    // COMPONENTS
    VBox componentsVBox;
    
    HBox classHBox;
    Label classLabel;
    TextField classField;
    
    HBox packageHBox;
    Label packageLabel;
    TextField packageField;
    
    HBox parentHBox;
    Label parentLabel;
    ComboBox<String> parentComboBox;
    
    HBox interfacesHBox;
    Label interfacesLabel;
    CheckComboBox<String> interfacesComboBox;
    
    VBox variablesBox;
    HBox variablesToolbar;
    Label variablesLabel;
    Button variablesAdd;
    Button variablesRemove;
    ScrollPane variablesScrollPane;
    TableView<VariableObj> variablesTableView;
    TableColumn varNameCol, varTypeCol, varAccessCol;
    TableColumn varStaticCol;
    
    VBox methodsBox;
    HBox methodsToolbar;
    Label methodsLabel;
    Button methodsAdd;
    Button methodsRemove;
    ScrollPane methodsScrollPane;
    TableView<MethodObj> methodsTableView;
    TableColumn methNameCol, methReturnCol, methAccessCol;
    TableColumn methStaticCol, methAbstractCol;
    ArrayList<TableColumn> methArgsColArray;
    

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        layoutGUI();
        setHandlers();
    }
    
    private void layoutGUI() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // BUILD THE DATAMANAGER COMPONENT
	dataManager = (DataManager) app.getDataComponent();
        
        // TOP TOOLBAR
        topToolbar = gui.getToolbar();
        
        // EDIT TOOLBAR
        editToolbar = new HBox();
        editToolbar.setMinWidth(504);
        editToolbar.setPadding(new Insets(40));
        selectButton = gui.initChildButton(editToolbar, SELECT_EDIT_ICON.toString(), SELECT_EDIT_TOOLTIP.toString(), true);
        resizeButton = gui.initChildButton(editToolbar, RESIZE_EDIT_ICON.toString(), RESIZE_EDIT_TOOLTIP.toString(), true);
        addClassButton = gui.initChildButton(editToolbar, ADDCLASS_EDIT_ICON.toString(), ADDCLASS_EDIT_TOOLTIP.toString(), true);
        addInterfaceButton = gui.initChildButton(editToolbar, ADDINTERFACE_EDIT_ICON.toString(), ADDINTERFACE_EDIT_TOOLTIP.toString(), true);
        removeButton = gui.initChildButton(editToolbar, REMOVE_EDIT_ICON.toString(), REMOVE_EDIT_TOOLTIP.toString(), true);
        undoButton = gui.initChildButton(editToolbar, UNDO_EDIT_ICON.toString(), UNDO_EDIT_TOOLTIP.toString(), true);
        redoButton = gui.initChildButton(editToolbar, REDO_EDIT_ICON.toString(), REDO_EDIT_TOOLTIP.toString(), true);
        
        // VIEW TOOLBAR
        viewToolbar = new HBox();
        viewToolbar.setMinWidth(216);
        viewToolbar.setPadding(new Insets(40));
        zoomInButton = gui.initChildButton(viewToolbar, ZOOMIN_VIEW_ICON.toString(), ZOOMIN_VIEW_TOOLTIP.toString(), true);
        zoomOutButton = gui.initChildButton(viewToolbar, ZOOMOUT_VIEW_ICON.toString(), ZOOMOUT_VIEW_TOOLTIP.toString(), true);
        checkBoxes = new VBox();
        gridCheckBox = new CheckBox(props.getProperty(GRID_VIEW_TEXT));
        snapCheckBox = new CheckBox(props.getProperty(SNAP_VIEW_TEXT));
        gridCheckBox.setDisable(true);
        snapCheckBox.setDisable(true);
        checkBoxes.setPadding(new Insets(10));
        checkBoxes.getChildren().addAll(gridCheckBox, new Label(""), snapCheckBox);
        viewToolbar.getChildren().add(checkBoxes);
        
        topToolbar.getChildren().addAll(editToolbar, viewToolbar);
        
        // COMPONENTS REGION
        componentsVBox = new VBox();
        
        classLabel = new Label("Class Name:");
        classLabel.setMinWidth(150);
        classField = new TextField();
        classField.setMinWidth(40);
        classHBox = new HBox();
        classHBox.setSpacing(160);
        classHBox.setPadding(new Insets(10));
        classHBox.getChildren().addAll(classLabel, classField);
        
        packageLabel = new Label("Package:");
        packageLabel.setMinWidth(150);
        packageField = new TextField();
        packageField.setMinWidth(40);
        packageField.setText("<default package>");
        packageHBox = new HBox();
        packageHBox.setSpacing(160);
        packageHBox.setPadding(new Insets(10));
        packageHBox.getChildren().addAll(packageLabel, packageField);
        
        parentLabel = new Label("Parent:");
        parentLabel.setMinWidth(150);
        parentComboBox = new ComboBox(dataManager.getParents());
        parentComboBox.setMinWidth(40);
        parentHBox = new HBox();
        parentHBox.setSpacing(160);
        parentHBox.setPadding(new Insets(10));
        parentHBox.getChildren().addAll(parentLabel, parentComboBox);
        
        interfacesHBox = new HBox();
        updateInterfaces(null);
        
        // BUILD THE COMPONENT REGIONS FOR VARIABLES AND METHODS
        initVariables();
        initMethods();
        
        componentsVBox.setMinWidth(500);
        componentsVBox.setMaxWidth(500);
        componentsVBox.setMinHeight(800);
        componentsVBox.setMaxHeight(800);
        
        componentsVBox.getChildren().addAll(classHBox, packageHBox, spacer(),
                parentHBox, interfacesHBox, spacer(),
                variablesBox, spacer(),
                methodsBox, spacer());
        
	// AND NOW SETUP THE WORKSPACE
	workspace = new Pane();
        
        workspaceSplitPane = new SplitPane();        
        workspaceSplitPane.getItems().addAll(dataManager.getScrollPane(), componentsVBox);
        
	workspace.getChildren().add(workspaceSplitPane);
    }
    
    private void initVariables() {
        // BUILD THE CONTROLS
        variablesBox = new VBox();
        variablesBox.setPadding(new Insets(10));
        variablesToolbar = new HBox(10);
        
        variablesLabel = new Label("Variables:");
        variablesToolbar.getChildren().add(variablesLabel);
        variablesAdd = gui.initChildButton(variablesToolbar, COMPONENT_ADD_ICON.toString(), COMPONENT_ADD_TOOLTIP.toString(), true);
        variablesRemove = gui.initChildButton(variablesToolbar, COMPONENT_REMOVE_ICON.toString(), COMPONENT_REMOVE_TOOLTIP.toString(), true);
        
        variablesTableView = new TableView();
        variablesScrollPane = new ScrollPane();
        variablesScrollPane.setContent(variablesTableView);
        
        variablesBox.getChildren().addAll(variablesToolbar, variablesScrollPane);
        
        // NOW SETUP THE TABLE COLUMNS
        varNameCol = new TableColumn("Name");
        varTypeCol = new TableColumn("Type");
        varStaticCol = new TableColumn("Static");
        varAccessCol = new TableColumn("Access");
        
        // AND LINK THE COLUMNS TO THE DATA
        varNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        varTypeCol.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        varStaticCol.setCellValueFactory(new PropertyValueFactory<Boolean, Boolean>("isStatic"));
        varStaticCol.setCellFactory(tc -> new CheckBoxTableCell<>());
        varAccessCol.setCellValueFactory(new PropertyValueFactory<String, String>("access"));
        
        variablesTableView.getColumns().addAll(varNameCol, varTypeCol, varStaticCol, varAccessCol);
        
        variablesTableView.setItems(dataManager.getSelected().getVariables());
        variablesTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        variablesTableView.setMinWidth(460);
        variablesTableView.setMinHeight(200);
    }
    
    private void initMethods() {
        // BUILD THE CONTROLS
        methodsBox = new VBox();
        methodsBox.setPadding(new Insets(10));
        methodsToolbar = new HBox(10);
        
        methodsLabel = new Label("Methods:");
        methodsToolbar.getChildren().add(methodsLabel);
        methodsAdd = gui.initChildButton(methodsToolbar, COMPONENT_ADD_ICON.toString(), COMPONENT_ADD_TOOLTIP.toString(), true);
        methodsRemove = gui.initChildButton(methodsToolbar, COMPONENT_REMOVE_ICON.toString(), COMPONENT_REMOVE_TOOLTIP.toString(), true);
        
        methodsTableView = new TableView();
        methodsScrollPane = new ScrollPane();
        methodsScrollPane.setContent(methodsTableView);
        
        methodsBox.getChildren().addAll(methodsToolbar, methodsScrollPane);
        
        // NOW SETUP THE TABLE COLUMNS
        methNameCol = new TableColumn("Name");
        methReturnCol = new TableColumn("Return");
        methStaticCol = new TableColumn("Static");
        methAbstractCol = new TableColumn("Abstract");
        methAccessCol = new TableColumn("Access");
        
        // ARRAYLIST OF TABLE COLUMNS FOR THE ARGUMENTS
        
        // AND LINK THE COLUMNS TO THE DATA
        methNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        methReturnCol.setCellValueFactory(new PropertyValueFactory<String, String>("returnType"));
        methStaticCol.setCellValueFactory(new PropertyValueFactory<Boolean, Boolean>("isStatic"));
        methStaticCol.setCellFactory(tc -> new CheckBoxTableCell<>());
        methAbstractCol.setCellValueFactory(new PropertyValueFactory<Boolean, Boolean>("isAbstract"));
        methAbstractCol.setCellFactory(tc -> new CheckBoxTableCell<>());
        methAccessCol.setCellValueFactory(new PropertyValueFactory<String, String>("access"));
        
        methodsTableView.getColumns().addAll(methNameCol, methReturnCol, methStaticCol, methAbstractCol, methAccessCol);
        
        methodsTableView.setItems(dataManager.getSelected().getMethods());
        methodsTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        methodsTableView.setMinWidth(460);
        methodsTableView.setMinHeight(200);
    }
    
    private Label spacer() {
        return new Label("\n\n\n");
    }
    
    private void setHandlers() {
        EditDesignTools editTools = new EditDesignTools(app);
        ViewTools viewTools = new ViewTools(app);
        ComponentTools componentTools = new ComponentTools(app);
        
        // HANDLING ALL EDIT/DESIGN RELATED ACTIONS
        
        selectButton.setOnAction(e -> {
            editTools.handleSelect();
        });
        
        resizeButton.setOnAction(e -> {
            editTools.handleResize();
        });
        
        addClassButton.setOnAction(e -> {
            editTools.handleAddClass();
        });
        
        addInterfaceButton.setOnAction(e -> {
            editTools.handleAddInterface();
        });
        
        removeButton.setOnAction(e -> {
            editTools.handleRemove();
        });
        
        undoButton.setOnAction(e -> {
            editTools.handleUndo();
        });
        
        redoButton.setOnAction(e -> {
            editTools.handleRedo();
        });
        
        // HANDLING ALL VIEW RELATED ACTIONS
        
        zoomInButton.setOnAction(e -> {
            viewTools.handleZoomIn();
        });
        
        zoomOutButton.setOnAction(e -> {
            viewTools.handleZoomOut();
        });
        
        gridCheckBox.setOnAction(e -> {
            viewTools.handleToggleRendering(gridCheckBox.isSelected());
        });
        
        snapCheckBox.setOnAction(e -> {
            viewTools.handleToggleSnapping(snapCheckBox.isSelected());
        });
        
        // HANDLING ALL COMPONENT TOOLS RELATED EVENTS
        
        classField.textProperty().addListener(e -> {
            componentTools.handleEditName(classField.getText());
        });
        
        packageField.textProperty().addListener(e -> {
            componentTools.handleEditPackage(packageField.getText());
        });
        
        parentComboBox.valueProperty().addListener(e -> {
            if(parentComboBox.isFocused()) {
                dataManager.editParent(parentComboBox.getValue());
            }
        });
        
        variablesAdd.setOnAction(e -> {
            if(dataManager.hasSelected()) {
                componentTools.handleAddVariable();
            }
        });
        
        variablesRemove.setOnAction(e -> {
            if(dataManager.hasSelected()) {
                VariableObj var = variablesTableView.getSelectionModel().getSelectedItem();
                
                if(var != null) {
                    componentTools.handleRemoveVariable(var);
                }
            }
        });
        
        methodsAdd.setOnAction(e -> {
            if(dataManager.hasSelected()) {
                componentTools.handleAddMethod();
            }
        });
        
        methodsRemove.setOnAction(e -> {
            if(dataManager.hasSelected()) {
                MethodObj meth = methodsTableView.getSelectionModel().getSelectedItem();
                
                if(meth != null) {
                    componentTools.handleRemoveMethod(meth);
                }
            }
        });
        
        // HANDLING TABLEVIEW COMPONENT EVENTS
        
        variablesTableView.setOnMouseClicked(e -> {
            if(dataManager.hasSelected()) {
                if (e.getClickCount() == 2) {
                    VariableObj var = variablesTableView.getSelectionModel().getSelectedItem();
                    
                    if(var != null) {
                        componentTools.handleEditVariable(var);
                    }
                }
            }
        });
        
        methodsTableView.setOnMouseClicked(e -> {
            if(dataManager.hasSelected()) {
                if (e.getClickCount() == 2) {
                    MethodObj meth = methodsTableView.getSelectionModel().getSelectedItem();
                    
                    if(meth != null) {
                        componentTools.handleEditMethod(meth);
                    }
                }
            }
        });
    }
    
    public void updateComponents(ClassObj selected) {
        if(selected != null) {
            classField.setText(selected.getName());
            packageField.setText(selected.getPackage());
            parentComboBox.setItems(dataManager.getParents());
            parentComboBox.setValue(selected.getParent());
            
            updateInterfaces(selected);
            
            variablesTableView.setItems(selected.getVariables());
            methodsTableView.setItems(selected.getMethods());
            
            variablesAdd.setDisable(false);
            variablesRemove.setDisable(false);
            methodsAdd.setDisable(false);
            methodsRemove.setDisable(false);
        } else {
            classField.setText("");
            packageField.setText("");
            parentComboBox.setValue("");
            parentComboBox.setItems(null);
            variablesTableView.setItems(null);
            methodsTableView.setItems(null);

            variablesAdd.setDisable(true);
            variablesRemove.setDisable(true);
            methodsAdd.setDisable(true);
            methodsRemove.setDisable(true);
        }
    }
    
    private void updateInterfaces(ClassObj selected) {
        interfacesHBox.getChildren().clear();
        
        interfacesLabel = new Label("Interfaces:");
        interfacesLabel.setMinWidth(150);
        interfacesLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        
        interfacesComboBox = new CheckComboBox(dataManager.getInterfaces());
        interfacesComboBox.setMinWidth(40);
        
        if(selected != null) {
            ObservableList<String> allInterfaces = interfacesComboBox.getItems();
            IndexedCheckModel<String> interfacesModel = interfacesComboBox.getCheckModel();
            
            for(int i = 0; i < allInterfaces.size(); i++) {
                if(selected.getInterfaces().contains(allInterfaces.get(i))) {
                    interfacesModel.check(i);
                }
            }
        }
        
        interfacesComboBox.getCheckModel().getCheckedIndices().addListener((ListChangeListener.Change<? extends Integer> c) -> {
            if(selected != null) {
                ObservableList<String> allInterfaces = interfacesComboBox.getItems();
                ArrayList<String> newInterfaces = new ArrayList();
                ObservableList<Integer> checkedInterfaces = interfacesComboBox.getCheckModel().getCheckedIndices();
                
                for(int i = 0; i < checkedInterfaces.size(); i++) {
                    newInterfaces.add(allInterfaces.get(checkedInterfaces.get(i)));
                }
                
                dataManager.setInterfaces(newInterfaces);
            }
        });
        
        interfacesHBox.setSpacing(160);
        interfacesHBox.setPadding(new Insets(10));
        interfacesHBox.getChildren().addAll(interfacesLabel, interfacesComboBox);
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        // BUTTONS
	editToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
	viewToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
	selectButton.getStyleClass().add(CLASS_FILE_BUTTON);
	resizeButton.getStyleClass().add(CLASS_FILE_BUTTON);
	addClassButton.getStyleClass().add(CLASS_FILE_BUTTON);
	addInterfaceButton.getStyleClass().add(CLASS_FILE_BUTTON);
	removeButton.getStyleClass().add(CLASS_FILE_BUTTON);
	undoButton.getStyleClass().add(CLASS_FILE_BUTTON);
	redoButton.getStyleClass().add(CLASS_FILE_BUTTON);
	zoomInButton.getStyleClass().add(CLASS_FILE_BUTTON);
	zoomOutButton.getStyleClass().add(CLASS_FILE_BUTTON);
	checkBoxes.getStyleClass().add(CLASS_FILE_BUTTON);
	gridCheckBox.getStyleClass().add(CLASS_FILE_BUTTON);
	snapCheckBox.getStyleClass().add(CLASS_FILE_BUTTON);
        
        // COMPONENTS
        componentsVBox.getStyleClass().add(CLASS_COMPONENTS_AREA);
        classLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        packageLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        parentLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        variablesLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        methodsLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        // EDIT/DESIGN BUTTONS
        selectButton.setDisable(false);
        resizeButton.setDisable(false);
        addClassButton.setDisable(false);
        addInterfaceButton.setDisable(false);
        removeButton.setDisable(false);
        undoButton.setDisable(true);
        redoButton.setDisable(true);

        // VIEW BUTTONS
        zoomInButton.setDisable(true);
        zoomOutButton.setDisable(true);
        checkBoxes.setDisable(false);
        gridCheckBox.setDisable(false);
        snapCheckBox.setDisable(false);
    }
}
