package jcd.objects;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.json.JsonObject;

/**
 * All Class related objects in jClassDesigner are descended from this object.
 *
 * @author Nathan
 */
public abstract class JCDObject {
    // ATTRIBUTES
    protected final StringProperty name = new SimpleStringProperty();
    
    // CONSTRUCTORS
    
    public JCDObject() {
        this.name.setValue("");
    }
    
    public JCDObject(String name) {
        this.name.setValue(name);
    }
    
    // ACCESSORS
    
    public String getName() {
        return name.getValue();
    }
    
    public StringProperty nameProperty() {
        return name;
    }
    
    // MUTATORS
    
    public void setName(String name) {
        this.name.setValue(name);
    }
    
    // METHODS
    
    protected static String fixStr(String str) {
        return str.substring(1, str.length() - 1);
    }
    
    protected String tab() {
        return "    ";
    }
    
    @Override
    public String toString() {
        return "Name: " + name.getValue();
    }
    
    // ABSTRACT METHODS
    
    public abstract String toCode();
    public abstract String toJavadoc();
    public abstract JsonObject toJSON();
    
    // IN ADDITION, EACH CLASS SHOULD HAVE A STATIC METHOD:
    // public static <Class> fromJSON(JsonObject)
}
