/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.objects;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 * This class contains data required to represent a method in a UML Class diagram
 *
 * @author Nathan Wood
 * @version 1.0
 */
public class MethodObj extends JCDObject implements Comparable {
    // JSON CONSTANTS
    private static final String 
            JSON_METHOD_NAME        = "meth_name",
            JSON_METHOD_RETURNTYPE  = "return_type",
            JSON_METHOD_ACCESS      = "access",
            JSON_METHOD_ISSTATIC    = "is_static",
            JSON_METHOD_ISABSTRACT  = "is_abstract",
            JSON_METHOD_ARGS        = "args";
    
    // ATTRIBUTES
    private final StringProperty
            returnType = new SimpleStringProperty(),
            access = new SimpleStringProperty();
    private final BooleanProperty
            isStatic = new SimpleBooleanProperty(),
            isAbstract = new SimpleBooleanProperty();
    private ObservableList<VariableObj> args;
    
    // CONSTRUCTORS
    public MethodObj() {
        super();
        this.returnType.setValue("void");
        this.access.setValue("public");
        this.isStatic.setValue(false);
        this.isAbstract.setValue(false);
        this.args = FXCollections.observableArrayList();
    }
    
    public MethodObj(String name, String returnType, String access) {
        super(name);
        this.returnType.setValue(returnType);
        this.access.setValue(access.toLowerCase());
        this.isStatic.setValue(false);
        this.isAbstract.setValue(false);
        this.args = FXCollections.observableArrayList();
    }
    
    public MethodObj(String name, String returnType, String access, ObservableList<VariableObj> args) {
        super(name);
        this.returnType.setValue(returnType);
        this.access.setValue(access.toLowerCase());
        this.isStatic.setValue(false);
        this.isAbstract.setValue(false);
        this.args = args;
    }
    
    public MethodObj(String name, String returnType, String access, boolean isStatic, boolean isAbstract, VariableObj arg) {
        super(name);
        this.returnType.setValue(returnType);
        this.access.setValue(access.toLowerCase());
        this.isStatic.setValue(isStatic);
        this.isAbstract.setValue(isAbstract);
        this.args = FXCollections.observableArrayList();
        
        this.args.add(arg);
    }
    
    public MethodObj(String name, String returnType, String access, boolean isStatic, boolean isAbstract, ObservableList<VariableObj> args) {
        super(name);
        this.returnType.setValue(returnType);
        this.access.setValue(access.toLowerCase());
        this.isStatic.setValue(isStatic);
        this.isAbstract.setValue(isAbstract);
        this.args = args;
    }
    
    // ACCESSORS
    
    public String getReturnType() {
        return returnType.getValue();
    }
    
    public String getAccess() {
        return access.getValue();
    }
    
    public boolean isStatic() {
        return isStatic.getValue();
    }
    
    public boolean isAbstract() {
        return isAbstract.getValue();
    }
    
    public VariableObj getArg(int index) {
        return args.get(index);
    }
    
    public ObservableList<VariableObj> getArgs() {
        return args;
    }
    
    public int argsSize() {
        return args.size();
    }
    
    public StringProperty returnTypeProperty() {
        return returnType;
    }
    
    public StringProperty accessProperty() {
        return access;
    }
    
    public BooleanProperty isStaticProperty() {
        return isStatic;
    }
    
    public BooleanProperty isAbstractProperty() {
        return isAbstract;
    }
    
    // MUTATORS    
    public void setReturnType(String returnType) {
        this.returnType.setValue(returnType);
    }
    
    public void setAccess(String access) {
        this.access.setValue(access.toLowerCase());
    }
    
    public void setIsStatic(boolean isStatic) {
        this.isStatic.setValue(isStatic);
    }
    
    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract.setValue(isAbstract);
    }
    
    public void setArg(int index, VariableObj arg) {
        args.set(index, arg);
    }
    
    public void setArgs(ObservableList<VariableObj> args) {
        this.args = args;
    }
    
    // METHODS
    public void addArg(VariableObj arg) {
        args.add(arg);
    }
    
    public boolean removeArg(VariableObj arg) {
        return args.remove(arg);
    }
    
    public VariableObj removeArg(int index) {
        return args.remove(index);
    }
    
    @Override
    public String toCode() {
        String output = "";
        
        output += this.toJavadoc();
        
        output += "\n" + tab() + access.getValue() + " ";
        
        if(isAbstract.getValue()) {
            output += "abstract ";
        } else if(isStatic.getValue()) {
            output += "static ";
        }
        
        if(!returnType.getValue().equals("")) {
            output += returnType.getValue() + " " + name.getValue() + "(";
        } else {
            output += name.getValue() + "(";
        }
        
        // Get each argument and add it to the string
        for(int i = 0; i < args.size(); i++) {
            output += args.get(i).toArg() + ", ";
            
            if(i == args.size() - 1) {
                // Trim off extra ", "
                output = output.substring(0, output.length() - 2);
            }
        }
        
        if(isAbstract.getValue()) {
            output += ");";
        } else {
            output += ") {";
            
            if(!returnType.getValue().equals("void") && !returnType.getValue().equals("")) {
                output += " return " + returnStatement() + "; }";
            } else {
                output += "}";
            }
        }
        
        return output;
    }
    
    private String returnStatement() {
        switch(returnType.getValue()) {
            case "boolean": return "false";
            case "int":     return "0";
            case "long":    return "0";
            case "char":    return "''";
            case "double":  return "0.0";
            case "float":   return "0.0";
            case "String":  return "\"\"";
            default:        return "new " + returnType.getValue() + "()";
        }
    }
    
    @Override
    public String toJavadoc() {
        String output = "/**"
                + "\n" + tab() + " * Default description for " + name.getValue()
                + "\n" + tab() + " * ";
        
        for(VariableObj arg : args) {
            output += "\n" + tab() + " * " + arg.toJavadoc() + " "
                    + "\n" + tab() + " * ";
        }
        
        if(!returnType.get().equals("void")) {
            output += "\n" + tab() + " * @return "
                    + "\n" + tab() + " * ";
        }
        
        output += "\n" + tab() + " */";
        
        return output;
    }
    
    @Override
    public JsonObject toJSON() {
        JsonArrayBuilder argsArrayBuilder = Json.createArrayBuilder();
        
        for(VariableObj var : args) {
            argsArrayBuilder.add(var.toJSON());
        }
        
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_METHOD_NAME,          name.getValue())
                .add(JSON_METHOD_RETURNTYPE,    returnType.getValue())
                .add(JSON_METHOD_ACCESS,        access.getValue())
                .add(JSON_METHOD_ISSTATIC,      isStatic.getValue())
                .add(JSON_METHOD_ISABSTRACT,    isAbstract.getValue())
                .add(JSON_METHOD_ARGS, argsArrayBuilder.build())
                .build();
        return jso;
    }
    
    @Override
    public boolean equals(Object obj) {
        MethodObj other = (MethodObj)obj;
        
        if(name.getValue().equals(other.getName()) && returnType.getValue().equals(other.getReturnType())
                && access.getValue().equals(other.getAccess()) && isStatic.getValue() == other.isStatic()
                && isAbstract.getValue() == other.isAbstract() && args.size() == other.argsSize()) {
            
            for(int i = 0; i < args.size(); i++) {
                if(!args.get(i).equals(other.getArg(i))) {
                    return false;
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public String toString() {
        String output = "";
        
        switch(access.getValue()) {
            case "public": output += "+"; break;
            case "private": output += "-"; break;
            case "protected": output += "#"; break;
            default: break;
        }
        
        if(isStatic.getValue()) {
            output += "$";
        }
        
        output += name.getValue() + "(";
        
        for(int i = 0; i < args.size(); i++) {
            output += args.get(i).toString();
            
            if(i != args.size() - 1) {
                output += ", ";
            }
        }
        
        output += ")";
        
        if(!returnType.getValue().equals("")) {
            output += " : " + returnType.getValue();
        }
        
        if(isAbstract.getValue()) {
            output += " {abstract}";
        }
        
        return output;
    }
    
    @Override
    public int compareTo(Object o) {
        MethodObj other = (MethodObj)o;
        
        return name.getValue().compareTo(other.getName());
    }
    
    // STATIC METHODS
    public static MethodObj fromJSON(JsonObject obj) {
        MethodObj method = new MethodObj();
                
        // LOAD BASIC PROPERTIES
        method.setName(fixStr(obj.getJsonString(JSON_METHOD_NAME).toString()));
        method.setReturnType(fixStr(obj.getJsonString(JSON_METHOD_RETURNTYPE).toString()));
        method.setAccess(fixStr(obj.getJsonString(JSON_METHOD_ACCESS).toString()));
        method.setIsStatic(obj.getBoolean(JSON_METHOD_ISSTATIC));
        method.setIsAbstract(obj.getBoolean(JSON_METHOD_ISABSTRACT));

        // LOAD ARGUMENT VARIABLES
        JsonArray variablesObjArray = obj.getJsonArray(JSON_METHOD_ARGS);
        
        for(int i = 0; i < variablesObjArray.size(); i++) {
            method.addArg(VariableObj.fromJSON(variablesObjArray.getJsonObject(i)));
        }
        
        // RETURN NEWLY CONSTRUCTED METHOD
        return method;
    }
}
