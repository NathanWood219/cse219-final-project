package jcd.objects;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.json.Json;
import javax.json.JsonObject;

/**
 * This class contains data required to represent a variable in a UML Class diagram
 *
 * @author Nathan Wood
 * @version 1.0
 */
public class VariableObj extends JCDObject implements Comparable {
    // JSON CONSTANTS
    private static final String
            JSON_VARIABLE_NAME      = "var_name",
            JSON_VARIABLE_TYPE      = "type",
            JSON_VARIABLE_ACCESS    = "access",
            JSON_VARIABLE_ISSTATIC  = "is_static";
    
    // ATTRIBUTES
    private final StringProperty
            type = new SimpleStringProperty(),
            access = new SimpleStringProperty();
    private final BooleanProperty
            isStatic = new SimpleBooleanProperty();
    
    // CONSTRUCTORS
    public VariableObj() {
        super();
        this.type.setValue("");
        this.access.setValue("<default>");
        this.isStatic.setValue(false);
    }
    
    public VariableObj(String name, String type) {
        super(name);
        this.type.setValue(type);
        this.access.setValue("<default>");
        this.isStatic.setValue(false);
    }
    
    public VariableObj(String name, String type, String access) {
        super(name);
        this.type.setValue(type);
        this.access.setValue(access.toLowerCase());
        this.isStatic.setValue(false);
    }
    
    public VariableObj(String name, String type, String access, boolean isStatic) {
        super(name);
        this.type.setValue(type);
        this.access.setValue(access.toLowerCase());
        this.isStatic.setValue(isStatic);
    }
    
    // ACCESSORS    
    public String getType() {
        return type.getValue();
    }
    
    public String getAccess() {
        return access.getValue();
    }
    
    public boolean isStatic() {
        return isStatic.getValue();
    }
    
    // MUTATORS    
    public void setType(String type) {
        this.type.setValue(type);
    }
    
    public void setAccess(String access) {
        this.access.setValue(access.toLowerCase());
    }
    
    public void setIsStatic(boolean isStatic) {
        this.isStatic.setValue(isStatic);
    }
    
    // METHODS    
    public StringProperty typeProperty() {        
        return type;
    }
    
    public BooleanProperty isStaticProperty() {        
        return isStatic;
    }
    
    public StringProperty accessProperty() {        
        return access;
    }
    
    public String toClassString() {
        String output = "";
        
        switch(access.getValue()) {
            case "public": output += "+"; break;
            case "private": output += "-"; break;
            case "protected": output += "#"; break;
            case "<default>": break;
            default: break;
        }
        
        if(isStatic.getValue()) {
            output += "$";
        }
        
        output += toString();
        
        return output;
    }
    
    @Override
    public String toCode() {
        String output = access.getValue() + " ";
        
        if(isStatic.getValue()) {
            output += "static ";
        }
        
        output += type.getValue() + " " + name.getValue() + ";";
        
        return output;
    }
    
    public String toArg() {
        return type.getValue() + " " + name.getValue();
    }
    
    @Override
    public String toJavadoc() {
        return "@param " + name.getValue();
    }
    
    @Override
    public JsonObject toJSON() {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_VARIABLE_NAME,        name.getValue())
                .add(JSON_VARIABLE_TYPE,        type.getValue())
                .add(JSON_VARIABLE_ACCESS,      access.getValue())
                .add(JSON_VARIABLE_ISSTATIC,    isStatic.getValue())
                .build();
        return jso;
    }
    
    @Override
    public boolean equals(Object obj) {
        VariableObj other = (VariableObj)obj;
        
        return name.getValue().equals(other.getName()) && type.getValue().equals(other.getType())
                && access.getValue().equals(other.getAccess()) && isStatic.getValue() == other.isStatic();
    }
    
    @Override
    public String toString() {
        return name.getValue() + " : " + type.getValue();
    }
    
    @Override
    public int compareTo(Object o) {
        VariableObj other = (VariableObj)o;
        
        return name.getValue().compareTo(other.getName());
    }
    
    // STATIC METHODS
    public static VariableObj fromJSON(JsonObject obj) {
        VariableObj variable = new VariableObj();

        // LOAD BASIC PROPERTIES
        variable.setName(fixStr(obj.getJsonString(JSON_VARIABLE_NAME).toString()));
        variable.setType(fixStr(obj.getJsonString(JSON_VARIABLE_TYPE).toString()));
        variable.setAccess(fixStr(obj.getJsonString(JSON_VARIABLE_ACCESS).toString()));
        variable.setIsStatic(obj.getBoolean(JSON_VARIABLE_ISSTATIC));
        
        // RETURN NEWLY CONSTRUCTED VARIABLE
        return variable;
    }
}
