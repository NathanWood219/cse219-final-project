package jcd.objects;

/**
 *
 * @author Nathan
 */
public enum NodeType {
    ORIGIN, NODE, ARROW;
}
