package jcd.objects;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import static saf.components.AppStyleArbiter.CLASS_VBOX_HEADER;
import static saf.components.AppStyleArbiter.CLASS_VBOX_LABEL;
import static saf.components.AppStyleArbiter.CLASS_VBOX_OBJECT;

/**
 * This class represents a UML Class Diagram
 *
 * @author Nathan Wood
 * @version 1.0
 */
public class ClassObj extends JCDObject {
    // Key: ClassName, Value: Package
    public static HashMap<String,String> packages = new HashMap();
    
    // JSON CONSTANTS
    private static final String 
            JSON_CLASS_TYPE             = "type",
            JSON_CLASS_NAME             = "class_name",
            JSON_CLASS_PACKAGE          = "package",
            JSON_CLASS_PARENT           = "parent",
            JSON_CLASS_INTERFACES       = "interfaces",
            JSON_CLASS_INTERFACE        = "interface",
            JSON_CLASS_VARIABLES        = "variables",
            JSON_CLASS_METHODS          = "methods",
            JSON_CLASS_CONNECTIONS      = "connections",
            JSON_CLASS_IMPORTS          = "imports",
            JSON_CLASS_IMPORT           = "import",
            JSON_CLASS_X                = "x",
            JSON_CLASS_Y                = "y",
            JSON_CLASS_WIDTH            = "width",
            JSON_CLASS_HEIGHT           = "height";
    
    // RENDERING
    final int SELECTED_OFFSET       = 6;
    final int SELECTED_PADDING      = 15;
    final int TEXT_PADDING          = 2;
    final int LINE_HEIGHT           = 12;
    final int CHAR_WIDTH            = 6;
    
    // ATTRIBUTES
    private Type type;
    private String pack;
    private String parent;
    
    private ArrayList<String> interfaces;
    private ObservableList<VariableObj> variables;
    private ObservableList<MethodObj> methods;
    private ArrayList<ConnectorObj> connections;
    private ArrayList<String> imports;
    
    private double x, y, width, height;
    
    // CONSTRUCTORS
    public ClassObj(Type type) {
        super();
        this.type = type;
        
        defaultValues("<default package>");
    }
    
    public ClassObj(String name, Type type) {
        super(name);
        this.type = type;
        
        defaultValues("<default package>");
    }
    
    public ClassObj(String name, Type type, String pack) {
        super(name);
        this.type = type;
        
        defaultValues(pack);
    }
    
    private void defaultValues(String pack) {
        this.pack   = pack;
        this.parent = "<none>";
        
        this.interfaces     = new ArrayList();
        this.variables      = FXCollections.observableArrayList();
        this.methods        = FXCollections.observableArrayList();
        this.connections    = new ArrayList();
        this.imports        = new ArrayList();
        
        this.x      = 500;
        this.y      = 500;
        this.width  = 150;
        this.height = 75;
        
        sendPackage();
    }
    
    // ACCESSORS
    public Type getType() {
        return type;
    }
    
    public String getPackage() {
        return pack;
    }
    
    public String getParent() {
        return parent;
    }
    
    public int variablesSize() {
        return variables.size();
    }
    
    public int methodsSize() {
        return methods.size();
    }
    
    public int connectionsSize() {
        return connections.size();
    }
    
    public int importsSize() {
        return imports.size();
    }
    
    public ArrayList<String> getInterfaces() {
        return interfaces;
    }
    
    public ObservableList<VariableObj> getVariables() {
        return variables;
    }
    
    public ObservableList<MethodObj> getMethods() {
        return methods;
    }
    
    public ArrayList<ConnectorObj> getConnections() {
        return connections;
    }
    
    public ArrayList<String> getImports() {
        return imports;
    }
    
    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    public double getWidth() {
        return width;
    }
    
    public double getHeight() {
        return height;
    }
    
    // MUTATORS
    public void setPackage(String pack) {
        clearPackage();
        
        this.pack = pack;
        
        sendPackage();
        
    }
    
    public void setParent(String parent) {        
        this.parent = parent;
    }
    
    public void setInterfaces(ArrayList<String> interfaces) {
        this.interfaces = interfaces;
    }
    
    public void setVariable(int index, VariableObj var) {
        variables.set(index, var);
    }
    
    public void setVariables(ObservableList<VariableObj> variables) {
        this.variables = variables;
    }
    
    public void setMethod(int index, MethodObj meth) {
        methods.set(index, meth);
    }
    
    public void setMethods(ObservableList<MethodObj> methods) {
        this.methods = methods;
    }
    
    public void setConnections(ArrayList<ConnectorObj> connections) {
        this.connections = connections;
    }
    
    public void setX(double x) {
        this.x = x;
    }
    
    public void setY(double y) {
        this.y = y;
    }
    
    public void setWidth(double width) {
        this.width = width;
    }
    
    public void setHeight(double height) {
        this.height = height;
    }
    
    // METHODS
    public void sendPackage() {
        packages.put(name.get(), pack);
    }
    
    public String clearPackage() {
        return packages.remove(name.get());
    }
    
    public ArrayList<ClassObj> updateConnections() {
        ArrayList<ClassObj> newClasses = buildClasses(variables, Arrow.AGGREGATION);
        
        ArrayList<String> inheritences = (ArrayList<String>)interfaces.clone();
        inheritences.add(parent);
        
        newClasses.addAll(buildClasses(inheritences, Arrow.INHERITENCE));
        
        for(MethodObj meth : methods) {
            newClasses.addAll(buildClasses(meth.getArgs(), Arrow.GENERALIZATION));
        }
        
        return newClasses;
    }
    
    private ArrayList<ClassObj> buildClasses(ObservableList<VariableObj> variables, Arrow arrow) {
        ArrayList<ClassObj> newClasses = new ArrayList();
        
        for(VariableObj var : variables) {
            String type = var.getType();
            boolean hasImport = false;
            
            if(!canBypassImport(type)) {
                for(ConnectorObj conn : connections) {
                    if(conn.getSource().equals(type)) {
                        hasImport = true;
                        break;
                    }
                }
            } else {
                hasImport = true;
            }
            
            if(!hasImport) {
                // MAKE CLASS
                
                ClassObj newClass = new ClassObj(type, Type.CLASS);
                
                newClass.setX(x + width + 40);
                newClass.setY(y);
                
                newClasses.add(newClass);
                
                // MAKE CONNECTION
                connect(newClass, arrow);
            }
        }
        
        return newClasses;
    }
    
    private ArrayList<ClassObj> buildClasses(ArrayList<String> names, Arrow arrow) {
        ArrayList<ClassObj> newClasses = new ArrayList();
        
        for(String name : names) {
            boolean hasImport = false;
            
            if(!canBypassImport(name)) {
                for(ConnectorObj conn : connections) {
                    if(conn.getSource().equals(name)) {
                        hasImport = true;
                        break;
                    }
                }
            } else {
                hasImport = true;
            }
            
            if(!hasImport) {
                ClassObj newClass = new ClassObj(name, Type.CLASS);
                
                newClass.setX(x + width + 60);
                newClass.setY(y);
                
                newClasses.add(newClass);
                
                // MAKE CONNECTION
                connect(newClass, arrow);
            }
        }
        
        return newClasses;
    }
    
    public void connect(ClassObj dest, Arrow arrow) {
        ConnectorObj connector = new ConnectorObj(arrow, dest);

        LineNode originNode = new LineNode(NodeType.ORIGIN, dest.getX(), dest.getY());
        LineNode arrowNode = new LineNode(NodeType.ARROW, x, y);

        connector.setOrigin(originNode);
        connector.setArrow(arrowNode);

        this.addConnection(connector);
    }
    
    public void addInterface(String name) {
        interfaces.add(name);
    }
    
    public boolean removeInterface(String name) {
        return interfaces.remove(name);
    }
    
    public void addVariable(VariableObj var) {
        variables.add(var);
        
        updateImports(var.getType());
    }
    
    public void editVariable(VariableObj oldObj, VariableObj newObj) {
        int index = variables.indexOf(oldObj);
        variables.set(index, newObj);
    }
    
    public boolean removeVariable(VariableObj var) {
        return variables.remove(var);
    }
    
    public VariableObj removeVariable(int index) {
        return variables.remove(index);
    }
    
    public void addMethod(MethodObj meth) {
        methods.add(meth);
        
        for(VariableObj arg : meth.getArgs()) {
            updateImports(arg.getType());
        }
    }
    
    public void editMethod(MethodObj oldObj, MethodObj newObj) {
        int index = methods.indexOf(oldObj);
        methods.set(index, newObj);
    }
    
    public boolean removeMethod(MethodObj meth) {
        return methods.remove(meth);
    }
    
    public MethodObj removeMethod(int index) {
        return methods.remove(index);
    }
    
    public void addConnection(ConnectorObj conn) {
        connections.add(conn);
        conn.setSource(name.get());
    }
    
    public boolean removeConnection(ConnectorObj conn) {
        return connections.remove(conn);
    }
    
    public ConnectorObj removeConnection(int index) {
        return connections.remove(index);
    }
    
    public void setImports(ArrayList<String> imports) {
        this.imports = imports;
    }
    
    public void checkForNewImports() {
        updateImports(parent);
        
        for(String str : interfaces) {
            updateImports(str);
        }
        
        for(VariableObj var : variables) {
            updateImports(var.getType());
        }
        
        for(MethodObj meth : methods) {
            for(VariableObj arg : meth.getArgs()) {
                updateImports(arg.getType());
            }
        }
    }
    
    public void updateImports(String type) {
        if(canBypassImport(type)) {
            return;
        }
        
        String foundPack = packages.get(name.getValue());
        
        if(foundPack != null && !foundPack.equals("<default package>")
                && !imports.contains(foundPack + "." + type)
                && !foundPack.equals(pack)) {
            
            imports.add(foundPack + "." + type);
        }
    }
    
    public boolean isAPI() {
        return (variables.isEmpty() && methods.isEmpty());
    }
    
    // REQUIRED METHODS
    @Override
    public String toCode() {
        checkForNewImports();
        
        String output = "";
        
        if(!pack.equals("<default package>")) {
            output += "package " + pack + ";\n\n";
        }
        
        for(String importStr : imports) {
            output += "import " + importStr + ";\n";
        }
        
        output += "\n" + this.toJavadoc() + "\n";
        
        output += "\npublic ";
        
        switch(type) {
            case CLASS      : output += "class "; break;
            case ABSTRACT   : output += "abstract class "; break;
            case INTERFACE  : output += "interface "; break;
        }
        
        output += name.get() + " ";
        
        if(!parent.isEmpty() && !parent.equals("<none>")) {
            output += "extends " + parent + " ";
        }
        
        if(!interfaces.isEmpty()) {
            output += "implements ";
            
            for(String str : interfaces) {
                output += str + ", ";
            }
            
            output = output.substring(0, output.length() - 2);
        }
        
        output += " {";
        
        for(VariableObj var : variables) {
            output += "\n" + tab() + var.toCode();
        }
        
        output += "\n";
        
        for(MethodObj meth : methods) {
            output += "\n" + tab() + meth.toCode() + "\n";
        }
        
        output += "\n}";
        
        return output;
    }
    
    private boolean canBypassImport(String type) {
        if(type == null) {
            return true;
        }
        
        String newType = "";
        
        for(int i = 0; i < type.length(); i++) {
            if(type.charAt(i) != '[' && type.charAt(i) != ']') {
                newType += type.charAt(i);
            }
        }
        
        type = newType;
        
        return (type.equals("int") || type.equals("double") || type.equals("long")
                || type.equals("float") || type.equals("String") || type.equals("char")
                || type.equals("void") || type.equals(name.getValue()) || type.equals("<none>"));
    }
    
    public String classToPackage(String name) {
        String foundPack = packages.get(name);
        
        if(foundPack == null || foundPack.equals("<default package>")) {
            return "";
        } else {
            return foundPack;
        }
    }
    
    public VBox render() {
        int extraLines = 4;
        int maxHorizontalChars = -1;

        // DRAWING CLASS

        VBox vbox = new VBox();

        vbox.setMinWidth(width);
        vbox.setMinHeight(height);
        vbox.relocate(x, y);

        // ADDING CLASS TYPE
        if(type != Type.CLASS) {
            String labelText = "";

            if(type == Type.ABSTRACT) {
                labelText = "{abstract}";
            } else if(type == Type.INTERFACE) {
                labelText = "<<interface>>";
            }

            Label typeLabel = new Label(labelText);
            typeLabel.getStyleClass().add(CLASS_VBOX_HEADER);
            //type.setPadding(new Insets(TEXT_PADDING));
            vbox.getChildren().add(typeLabel);
            extraLines++;
        }

        // ADDING CLASS NAME
        Label names = new Label(name.get());
        names.getStyleClass().add(CLASS_VBOX_HEADER);
        names.setPadding(new Insets(TEXT_PADDING));
        vbox.getChildren().add(names);

        if(!isAPI()) {
            
            vbox.getChildren().add(new Separator());

            // ADDING VARIABLES TEXT
            String variablesStr = "";

            for(VariableObj var : variables) {
                String varStr = var.toString();
                
                maxHorizontalChars = Math.max(maxHorizontalChars, varStr.length());
                
                variablesStr += var.toClassString() + "\n";
            }

            if(!variablesStr.equals("")) {
                variablesStr = variablesStr.substring(0, variablesStr.length() - 1);
            }

            Label varsLabel = new Label(variablesStr);
            varsLabel.getStyleClass().add(CLASS_VBOX_LABEL);
            varsLabel.setPadding(new Insets(TEXT_PADDING));
            vbox.getChildren().addAll(varsLabel, new Separator());

            // ADDING METHODS TEXT
            String methodsStr = "";

            for(MethodObj meth : methods) {
                String methStr = meth.toString();
                
                maxHorizontalChars = Math.max(maxHorizontalChars, methStr.length());
                
                methodsStr += meth.toString() + "\n";
            }

            if(!methodsStr.equals("")) {
                methodsStr = methodsStr.substring(0, methodsStr.length() - 1);
            }

            Label methodsLabel = new Label(methodsStr);
            methodsLabel.getStyleClass().add(CLASS_VBOX_LABEL);
            methodsLabel.setPadding(new Insets(TEXT_PADDING));
            vbox.getChildren().add(methodsLabel);
        } else {
            vbox.setAlignment(Pos.CENTER);
        }

        vbox.getStyleClass().add(CLASS_VBOX_OBJECT);

        height = (Math.max(height, (variables.size() + methods.size() + extraLines) * LINE_HEIGHT * 2));
        width = (Math.max(width, (maxHorizontalChars * CHAR_WIDTH)));
        vbox.setMinHeight(height);
        
        return vbox;
    }
    
    @Override
    public String toJavadoc() {
        return "/**"
                + "\n * Default description for " + name.get()
                + "\n * "
                + "\n * @author " + System.getProperty("user.name")
                + "\n * @version 1.0"
                + "\n */";
    }
    
    @Override
    public JsonObject toJSON() {
        JsonArrayBuilder interfacesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder variablesArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder methodsArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder connectionsArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder importsArrayBuilder = Json.createArrayBuilder();
        
        for(String str : interfaces) {
            interfacesArrayBuilder.add(Json.createObjectBuilder()
                .add(JSON_CLASS_INTERFACE,        str));
        }
        
        for(VariableObj var : variables) {
            variablesArrayBuilder.add(var.toJSON());
        }
        
        for(MethodObj meth : methods) {
            methodsArrayBuilder.add(meth.toJSON());
        }
        
        for(ConnectorObj conn : connections) {
            connectionsArrayBuilder.add(conn.toJSON());
        }
        
        for(String importStr : imports) {
            importsArrayBuilder.add(Json.createObjectBuilder()
                .add(JSON_CLASS_IMPORT,        importStr));
        }
        
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_CLASS_TYPE,           type.toString())
                .add(JSON_CLASS_NAME,           name.get())
                .add(JSON_CLASS_PACKAGE,        pack)
                .add(JSON_CLASS_PARENT,         parent)
                .add(JSON_CLASS_INTERFACES,     interfacesArrayBuilder.build())
                .add(JSON_CLASS_VARIABLES,      variablesArrayBuilder.build())
                .add(JSON_CLASS_METHODS,        methodsArrayBuilder.build())
                .add(JSON_CLASS_CONNECTIONS,    connectionsArrayBuilder.build())
                .add(JSON_CLASS_IMPORTS,        importsArrayBuilder.build())
                .add(JSON_CLASS_X,              x)
                .add(JSON_CLASS_Y,              y)
                .add(JSON_CLASS_WIDTH,          width)
                .add(JSON_CLASS_HEIGHT,         height)
                .build();
        return jso;
    }
    
    @Override
    public boolean equals(Object obj) {
        ClassObj other = (ClassObj)obj;
        
        if(!name.equals(other.getName()) || !pack.equals(other.getPackage())
                || !parent.equals(other.getParent())) {
            return false;
        }
        
        if(!variables.equals(other.getVariables()) || !methods.equals(other.getMethods())
                || !connections.equals(other.getConnections()) || !imports.equals(other.getImports())) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public String toString() {
        String output = "";
        
        if(type == Type.INTERFACE) {
            output += "<<interface>>\n";
        } else if(type == Type.ABSTRACT) {
            output += "<<abstract>>\n";
        }
        
        output += super.toString()
                + "\nPackage: " + pack
                + "\nParent: " + parent;
        
        for(VariableObj var : variables) {
            output += "\n" + var.toClassString();
        }
        
        for(MethodObj meth : methods) {
            output += "\n" + meth.toString();
        }
        
        for(ConnectorObj conn : connections) {
            output += "\n" + conn.toString();
        }
        
        return output;
    }
    
    public static ClassObj fromJSON(JsonObject obj) {
        // CONSTRUCT WITH FINAL TYPE
        ClassObj classObject = new ClassObj(Type.valueOf(fixStr(obj.getJsonString(JSON_CLASS_TYPE).toString())));
        
        // LOAD BASIC PROPERTIES
        classObject.setName(fixStr(obj.getJsonString(JSON_CLASS_NAME).toString()));
        classObject.setPackage(fixStr(obj.getJsonString(JSON_CLASS_PACKAGE).toString()));
        classObject.setParent(fixStr(obj.getJsonString(JSON_CLASS_PARENT).toString()));
        classObject.setX(obj.getJsonNumber(JSON_CLASS_X).doubleValue());
        classObject.setY(obj.getJsonNumber(JSON_CLASS_Y).doubleValue());
        classObject.setWidth(obj.getJsonNumber(JSON_CLASS_WIDTH).doubleValue());
        classObject.setHeight(obj.getJsonNumber(JSON_CLASS_HEIGHT).doubleValue());

        // LOAD VARIABLES, METHODS, AND CONNECTORS
        
        JsonArray interfacesObjArray = obj.getJsonArray(JSON_CLASS_INTERFACES);
        JsonArray variablesObjArray = obj.getJsonArray(JSON_CLASS_VARIABLES);
        JsonArray methodsObjArray = obj.getJsonArray(JSON_CLASS_METHODS);
        JsonArray connectionsObjArray = obj.getJsonArray(JSON_CLASS_CONNECTIONS);
        JsonArray importsObjArray = obj.getJsonArray(JSON_CLASS_IMPORTS);
        
        for(int i = 0; i < interfacesObjArray.size(); i++) {
            classObject.addInterface(fixStr(interfacesObjArray.getJsonObject(i).getJsonString(JSON_CLASS_INTERFACE).toString()));
        }
        
        for(int i = 0; i < variablesObjArray.size(); i++) {
            classObject.addVariable(VariableObj.fromJSON(variablesObjArray.getJsonObject(i)));
        }
        
        for(int i = 0; i < methodsObjArray.size(); i++) {
            classObject.addMethod(MethodObj.fromJSON(methodsObjArray.getJsonObject(i)));
        }
        
        for(int i = 0; i < connectionsObjArray.size(); i++) {
            classObject.addConnection(ConnectorObj.fromJSON(connectionsObjArray.getJsonObject(i)));
        }
        
        ArrayList<String> imports = new ArrayList();
        
        for(int i = 0; i < importsObjArray.size(); i++) {
            imports.add(fixStr(importsObjArray.getJsonObject(i).getJsonString(JSON_CLASS_IMPORT).toString()));
        }
        
        classObject.setImports(imports);
        
        // RETURN NEWLY CREATED CLASS
        return classObject;
    }
}
