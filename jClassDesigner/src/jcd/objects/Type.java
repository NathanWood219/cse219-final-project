package jcd.objects;

/**
 *
 * @author Nathan
 */
public enum Type {
    CLASS, INTERFACE, ABSTRACT;
}
