package jcd.objects;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author Nathan
 */
public class ConnectorObj {
    // JSON CONSTANTS
    private static final String
            JSON_CONNECTOR_TYPE = "conn_type",
            JSON_CONNECTOR_SOURCE = "source",
            JSON_CONNECTOR_DEST = "dest",
            JSON_CONNECTOR_ORIGIN = "origin",
            JSON_CONNECTOR_ARROW = "arrow",
            JSON_CONNECTOR_NODES = "line_nodes";
    
    // RENDERING
    final int LINE_DRAW_WIDTH       = 2;
    final int LINE_DETECT_WIDTH     = 4;
    final Double ARROW_WIDTH        = 30.0;
    final Double ARROW_HEIGHT       = 30.0;
    
    
    // ATTRIBUTES
    private final Arrow type;
    private String source, destination;
    private LineNode origin, arrow;
    private ArrayList<LineNode> nodes;
    
    // CONSTRUCTORS
    public ConnectorObj(Arrow type) {
        this.type = type;
        this.nodes = new ArrayList();
    }
    
    public ConnectorObj(Arrow type, String destination) {
        this.type = type;
        this.destination = destination;
        this.nodes = new ArrayList();
    }
    
    public ConnectorObj(Arrow type, ClassObj destClass) {
        this.type = type;
        this.destination = destClass.getName();
        this.nodes = new ArrayList();
    }
    
    // ACCESSORS
    public Arrow getType() {
        return type;
    }
    
    public String getSource() {
        return source;
    }
    
    public String getDestination() {
        return destination;
    }
    
    public LineNode getOrigin() {
        return origin;
    }
    
    public LineNode getArrow() {
        return arrow;
    }
    
    public int getNodesSize() {
        return nodes.size();
    }
    
    public LineNode getNode(int index) {
        return nodes.get(index);
    }
    
    public ArrayList<LineNode> getNodes() {
        return nodes;
    }
    
    // MUTATORS
    public void setSource(String source) {
        this.source = source;
    }
    
    public void setDestination(String destination) {
        this.destination = destination;
    }
    
    public void setOrigin(LineNode origin) {
        this.origin = origin;
    }
    
    public void setArrow(LineNode arrow) {
        this.arrow = arrow;
    }
    
    public void setNodes(ArrayList<LineNode> nodes) {
        this.nodes = nodes;
    }
    
    // METHODS
    public void moveNode(LineNode node, double x, double y) {
        if(node.equals(origin)) {
            origin.setX(x);
            origin.setY(y);
        } else if(node.equals(arrow)) {
            arrow.setX(x);
            arrow.setY(y);
        } else {
            for(LineNode lineNode : nodes) {
                if(lineNode.equals(node)) {
                    lineNode.setX(x);
                    lineNode.setY(y);
                    break;
                }
            }
        }
    }
    
    public void addNode(LineNode node) {
        nodes.add(node);
    }
    
    public boolean removeNode(LineNode node) {
        return nodes.remove(node);
    }
    
    public LineNode removeNode(int index) {
        return nodes.remove(index);
    }
    
    public JsonObject toJSON() {
        JsonArrayBuilder nodesArrayBuilder = Json.createArrayBuilder();
        
        for(LineNode node : nodes) {
            nodesArrayBuilder.add(node.toJSON());
        }
        
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_CONNECTOR_TYPE, type.toString())
                .add(JSON_CONNECTOR_SOURCE, source)
                .add(JSON_CONNECTOR_DEST, destination)
                .add(JSON_CONNECTOR_ORIGIN, origin.toJSON())
                .add(JSON_CONNECTOR_ARROW, arrow.toJSON())
                .add(JSON_CONNECTOR_NODES, nodesArrayBuilder.build())
                .build();
        return jso;
    }
    
    @Override
    public boolean equals(Object obj) {
        ConnectorObj other = (ConnectorObj)obj;
        
        if(type == other.getType() && source.equals(other.getSource())
                && destination.equals(other.getDestination())
                && nodes.size() == other.getNodesSize()) {
            
            for(int i = 0; i < nodes.size(); i++) {
                if(!nodes.get(i).equals(other.getNode(i))) {
                    return false;
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public String toString() {
        return type.toString() + ": " + source + " ---> " + destination;
    }
    
    // STATIC METHODS
    public static ConnectorObj fromJSON(JsonObject obj) {
        ConnectorObj connector = new ConnectorObj(Arrow.valueOf(JCDObject.fixStr(obj.getJsonString(JSON_CONNECTOR_TYPE).toString())));

        // LOAD BASIC PROPERTIES
        connector.setSource(JCDObject.fixStr(obj.getJsonString(JSON_CONNECTOR_SOURCE).toString()));
        connector.setDestination(JCDObject.fixStr(obj.getJsonString(JSON_CONNECTOR_DEST).toString()));

        // LOAD NODES
        connector.setOrigin(LineNode.fromJSON(obj.getJsonObject(JSON_CONNECTOR_ORIGIN)));
        connector.setArrow(LineNode.fromJSON(obj.getJsonObject(JSON_CONNECTOR_ARROW)));
        
        JsonArray nodesObjArray = obj.getJsonArray(JSON_CONNECTOR_NODES);
        
        for(int i = 0; i < nodesObjArray.size(); i++) {
            connector.addNode(LineNode.fromJSON(nodesObjArray.getJsonObject(i)));
        }
        
        // RETURN NEWLY CONSTRUCTED CONNECTOR
        return connector;
    }
}
