package jcd.objects;

import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author Nathan
 */
public class LineNode {
    // JSON CONSTANTS
    private static final String
            JSON_NODE_X = "node_x",
            JSON_NODE_Y = "node_y",
            JSON_NODE_TYPE = "node_type";
    
    // ATTRIBUTES
    private double x, y;
    private NodeType type;
    
    // CONSTRUCTORS
    public LineNode() {
        this.x = 0;
        this.y = 0;
        this.type = NodeType.NODE;
    }
    
    public LineNode(double x, double y) {
        this.x = x;
        this.y = y;
        this.type = NodeType.NODE;
    }
    
    public LineNode(NodeType type, double x, double y) {
        this.x = x;
        this.y = y;
        this.type = type;
    }
    
    // ACCESSORS
    public double x() {
        return x;
    }
    
    public double y() {
        return y;
    }
    
    public NodeType type() {
        return type;
    }
    
    // MUTATORS
    public void setX(double x) {
        this.x = x;
    }
    
    public void setY(double y) {
        this.y = y;
    }
    
    public void setType(NodeType type) {
        this.type = type;
    }
    
    // METHODS
    private static String fixStr(String str) {
        return str.substring(1, str.length() - 1);
    }
    
    public JsonObject toJSON() {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_NODE_X, x)
                .add(JSON_NODE_Y, y)
                .add(JSON_NODE_TYPE, type.toString())
                .build();
        return jso;
    }
    
    @Override
    public boolean equals(Object obj) {
        LineNode other = (LineNode)obj;
        
        return ((int)x == (int)other.x() && (int)y == (int)other.y());
    }
    
    @Override
    public String toString() {
        return "X: " + x + "; Y: " + y;
    }
    
    // STATIC METHODS
    public static LineNode fromJSON(JsonObject obj) {
        LineNode node = new LineNode();
                
        // LOAD BASIC PROPERTIES
        node.setX(obj.getJsonNumber(JSON_NODE_X).doubleValue());
        node.setY(obj.getJsonNumber(JSON_NODE_Y).doubleValue());
        node.setType(NodeType.valueOf(fixStr(obj.getJsonString(JSON_NODE_TYPE).toString())));
        
        // RETURN NEWLY CONSTRUCTED LINENODE
        return node;
    }
}
