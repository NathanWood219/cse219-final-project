package jcd.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import jcd.gui.Workspace;
import jcd.objects.Arrow;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import jcd.objects.ClassObj;
import jcd.objects.ConnectorObj;
import jcd.objects.LineNode;
import jcd.objects.MethodObj;
import jcd.objects.NodeType;
import jcd.objects.VariableObj;
import jcd.objects.Type;
import static saf.components.AppStyleArbiter.*;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    final String BACKGROUND_COLOR   = "fefce0";
    final String OUTLINE_COLOR      = "3f3f3d";
    final String CLASS_COLOR        = "ffffff";
    final String SELECTED_COLOR     = "f4eb49";
    final int SELECTED_WIDTH        = 25;
    
    // RENDERING
    final int LINE_DRAW_WIDTH       = 2;
    final int LINE_DETECT_WIDTH     = 4;
    final Double ARROW_WIDTH        = 15.0;
    
    // UI
    final int SCROLLPANE_WIDTH      = 1216;
    final int SCROLLPANE_HEIGHT     = 800;
    final int PANE_WIDTH            = SCROLLPANE_WIDTH * 4;
    final int PANE_HEIGHT           = SCROLLPANE_HEIGHT * 4;
    
    int paneScaleX                  = 1;
    int paneScaleY                  = 1;
    final double PANE_SCALE_FACTOR  = 1.2;
    
    // GRID RENDERING
    final int GRID_LINE_WIDTH       = 2;
    int gridCellWidth               = 32;
    int gridCellHeight              = 32;
    
    final Cursor
            CURSOR_DEFAULT  = Cursor.DEFAULT,
            CURSOR_SELECT   = Cursor.HAND,
            CURSOR_DRAG     = Cursor.CLOSED_HAND,
            CURSOR_DELETE   = Cursor.CROSSHAIR,
            CURSOR_PAN      = Cursor.CLOSED_HAND;
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    ScrollPane scrollPane;
    Pane pane;
    Canvas grid;
    
    ArrayList<ClassObj> classes, garbage;
    ClassObj selected;
    int selectedIndex;
    
    boolean canSelect, canResize, canDelete,            // States
            gridRender, gridSnap,                       // Toggled Features
            resizing, moving, panning;                  // Actives
    
    double mouseX, mouseY, xOffset, yOffset, prevHvalue, prevVvalue;
    
    // FOR TESTING PURPOSES ONLY
    public DataManager() {
        classes = new ArrayList();
    }

    /**
     * This constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     * 
     * @throws Exception
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        
        pane = new Pane();
        grid = buildGrid();
        
        classes = new ArrayList();
        garbage = new ArrayList();
        
        selected = new ClassObj(Type.CLASS);
        
        disableStates();
        
        gridRender = false;
        gridSnap = false;
        
        setupScrollPane();
        setupPane();
        
        pane.getChildren().add(grid);
        
        scrollPane.setContent(pane);
    }
    
    private void setupScrollPane() {
        scrollPane = new ScrollPane();
        scrollPane.setMinWidth(SCROLLPANE_WIDTH);
        scrollPane.setMinHeight(SCROLLPANE_HEIGHT);
        scrollPane.setMaxWidth(SCROLLPANE_WIDTH);
        scrollPane.setMaxHeight(SCROLLPANE_HEIGHT);
        scrollPane.setPrefSize(SCROLLPANE_WIDTH, SCROLLPANE_HEIGHT);
        
        scrollPane.setMinViewportWidth(SCROLLPANE_WIDTH);
        scrollPane.setMinViewportHeight(SCROLLPANE_HEIGHT);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        
        scrollPane.setHvalue(0.5);
        scrollPane.setVvalue(0.5);
        
        scrollPane.getStyleClass().add(CLASS_WORKSPACE_AREA);
    }
    
    public void setupPane() {
        pane = new Pane();
        
        pane.setMinWidth(PANE_WIDTH);
        pane.setMinHeight(PANE_HEIGHT);
        
        pane.setMaxWidth(PANE_WIDTH);
        pane.setMaxHeight(PANE_HEIGHT);
        
        pane.setScaleX(paneScaleX);
        pane.setScaleY(paneScaleY);
        
        pane.getStyleClass().add(CLASS_WORKSPACE_AREA);
        
        pane.setOnMousePressed(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
            mousePressed();
            update();
        });
        
        pane.setOnMouseReleased(e -> {
            mouseX = e.getX() + (3.0/4 * scrollPane.getHvalue() * PANE_WIDTH);
            mouseY = e.getY() + (3.0/4 * scrollPane.getVvalue() * PANE_HEIGHT) - 135;
            mouseReleased();
            update();
        });
        
        pane.setOnMouseDragged(e -> {
            mouseX = e.getX() + (3.0/4 * scrollPane.getHvalue() * PANE_WIDTH);
            mouseY = e.getY() + (3.0/4 * scrollPane.getVvalue() * PANE_HEIGHT) - 135;
            mouseDragged();
            update();
        });
    }

    public void update() {
        if(app == null) {
            return;
        }
        
        setupPane();
        scrollPane.setContent(pane);
        
        // DRAWING THE GRID
        if(gridRender) {
            pane.getChildren().add(grid);
        }
        
        int i = 0;
        for(ClassObj obj : classes) {
            
            // DRAWING CONNECTORS
            
            for(ConnectorObj conn : obj.getConnections()) {
                ArrayList<LineNode> drawNodes = (ArrayList<LineNode>)conn.getNodes().clone();
                
                drawNodes.add(0, conn.getOrigin());
                drawNodes.add(conn.getArrow());
                
                for(int k = 0; k < drawNodes.size() - 1; k++) {
                    Line line = new Line();
                    
                    line.setStartX(drawNodes.get(k).x());
                    line.setStartY(drawNodes.get(k).y());
                    
                    line.setEndX(drawNodes.get(k+1).x());
                    line.setEndY(drawNodes.get(k+1).y());
                    
                    line.setStrokeWidth(LINE_DRAW_WIDTH);
                    
                    pane.getChildren().add(line);
                }
                
                Shape arrow = buildConnectorArrow(conn.getType());
                
                arrow.relocate(conn.getArrow().x() - (ARROW_WIDTH / 2), conn.getArrow().y() - (ARROW_WIDTH / 2));
                
                pane.getChildren().add(arrow);
            }
            
            // DRAWING CLASSES
            
            VBox classBox = obj.render();
            
            if(i == selectedIndex) {
                DropShadow shadow = new DropShadow();
                shadow.setRadius(SELECTED_WIDTH);
                shadow.setColor(Color.web(SELECTED_COLOR));
                
                classBox.setEffect(shadow);
            }
            
            pane.getChildren().add(classBox);
            
            i++;
        }
    }
    
    private Shape buildConnectorArrow(Arrow arrow) {
        if(arrow == Arrow.AGGREGATION) {
            Rectangle rect = new Rectangle();
            
            rect.setWidth(ARROW_WIDTH);
            rect.setHeight(ARROW_WIDTH);
            
            rect.setRotate(45);
            
            rect.setFill(Color.web("#ffffff"));
            rect.setStroke(Color.web("#000000"));
            rect.setStrokeWidth(LINE_DRAW_WIDTH);
            
            return rect;
            
        } else if(arrow == Arrow.GENERALIZATION) {
            Polyline poly = new Polyline();
            
            poly.getPoints().addAll(new Double[]{
                0.0, 0.0,
                ARROW_WIDTH, ARROW_WIDTH / 2,
                0.0, ARROW_WIDTH
            });
            
            poly.setStroke(Color.web("#000000"));
            poly.setStrokeWidth(LINE_DRAW_WIDTH);
            
            return poly;
        } else if(arrow == Arrow.INHERITENCE) {
            Polygon poly = new Polygon();
            
            poly.getPoints().addAll(new Double[]{
                0.0, 0.0,
                ARROW_WIDTH, ARROW_WIDTH / 2,
                0.0, ARROW_WIDTH
            });
            
            poly.setFill(Color.web("#ffffff"));
            poly.setStroke(Color.web("#000000"));
            poly.setStrokeWidth(LINE_DRAW_WIDTH);
            
            return poly;
            
        } else {
            return null;
        }
    }
    
    public Canvas buildGrid() {
        Canvas canvas = new Canvas(PANE_WIDTH, PANE_HEIGHT);
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        
        graphics.setFill(Color.web("#BFBEAC"));
        graphics.setLineWidth(GRID_LINE_WIDTH);
        
        for(int i = 0; i < PANE_WIDTH / gridCellWidth; i++) {
            graphics.fillRect(i * gridCellWidth, 0, GRID_LINE_WIDTH, PANE_HEIGHT);
            graphics.beginPath();
        }
        
        for(int i = 0; i < PANE_HEIGHT / gridCellHeight; i++) {
            graphics.fillRect(0, i * gridCellHeight, PANE_WIDTH, GRID_LINE_WIDTH);
            graphics.beginPath();
        }
        
        return canvas;
    }
    
    public void cleanupConnections() {
        for(ClassObj classObj : classes) {
            for(ConnectorObj conn : classObj.getConnections()) {
                if(!classesContain(conn.getSource())) {
                    classObj.removeConnection(conn);
                }
            }
        }
    }
    
    private boolean classesContain(String name) {
        for(ClassObj classObj : classes) {
            if(classObj.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }
    
    public ClassObj getSelected() {
        return selected;
    }
    
    public boolean hasSelected() {
        return (selected != null);
    }
    
    public ScrollPane getScrollPane() {
        return scrollPane;
    }
    
    public ArrayList<ClassObj> getClasses() {
        return classes;
    }
    
    public void setClasses(ArrayList<ClassObj> classes) {
        this.classes = classes;
    }
    
    public void addClass() {
        selected = new ClassObj(Type.CLASS);
        buildClassObject();
    }
    
    public void addInterface() {
        selected = new ClassObj(Type.INTERFACE);
        buildClassObject();
    }
    
    private void buildClassObject() {
        classes.add(selected);
        
        selected.setX((3.0/4 * scrollPane.getHvalue() * PANE_WIDTH) + (SCROLLPANE_WIDTH / 2) - (selected.getWidth() / 2));
        selected.setY((3.0/4 * scrollPane.getVvalue() * PANE_HEIGHT) + (SCROLLPANE_HEIGHT / 2) - (selected.getHeight() / 2));
        
        selectedIndex = classes.size() - 1;
        enableSelect();
        
        update();
        dataChanged(true);
        
        // CLEAR THE COMPONENTS AREA
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.updateComponents(selected);
    }
    
    public ObservableList<String> getParents() {
        ObservableList<String> parents = FXCollections.observableArrayList();
        
        parents.add("<none>");
        
        for(ClassObj obj : classes) {
            if(selected == null || !selected.getName().equals(obj.getName())) {
                parents.add(obj.getName());
            }
        }
        
        return parents;
    }
    
    public ObservableList<String> getInterfaces() {
        ObservableList<String> interfaces = FXCollections.observableArrayList();
        
        for(ClassObj obj : classes) {
            if(obj.getType() == Type.INTERFACE && (selected == null || !selected.getName().equals(obj.getName()))) {
                interfaces.add(obj.getName());
            }
        }
        
        return interfaces;
    }
    
    public void remove(int index) {
        if(selectedIndex == index) {
            selected = null;
            selectedIndex = -1;
        }
        
        garbage.add(classes.remove(index));
        
        cleanupConnections();
        update();
        dataChanged(true);
        
        // UPDATE COMPONENTS
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.updateComponents(selected);
    }
    
    public void select(int index) {
        setCursor(CURSOR_SELECT);

        ClassObj obj = classes.get(index);

        selected = obj;
        selectedIndex = index;

        moving = true;

        xOffset = mouseX - obj.getX();
        yOffset = mouseY - obj.getY();
    }
    
    public void updateClasses() {
        ArrayList<ClassObj> newClasses = selected.updateConnections();
        
        for(ClassObj obj1 : newClasses) {
            boolean exists = false;
            
            if(obj1.getName().equals("<none>") || obj1.getName().isEmpty()) {
                continue;
            }
            
            for(ClassObj obj2 : classes) {
                if(obj1.getName().equals(obj2.getName())) {
                    exists = true;
                    break;
                }
            }
            
            for(ClassObj obj2 : garbage) {
                if(obj1.getName().equals(obj2.getName())) {
                    exists = true;
                    break;
                }
            }
            
            if(!exists) {
                classes.add(obj1);
            }
        }
        
        moveConnectors();
    }
    
    public void addVariable(VariableObj var) {
        selected.addVariable(var);
        
        updateClasses();
        dataChanged(true);
        update();
    }
    
    public void editVariable(VariableObj oldVar, VariableObj newVar) {
        selected.editVariable(oldVar, newVar);
        
        updateClasses();
        dataChanged(true);
        update();
    }
    
    public void removeVariable(int index) {
        selected.removeVariable(index);
        dataChanged(true);
        update();
    }
    
    public void removeVariable(VariableObj var) {
        selected.removeVariable(var);
        dataChanged(true);
        update();
    }
    
    public void addMethod(MethodObj meth) {
        selected.addMethod(meth);
        
        updateClasses();
        dataChanged(true);
        update();
    }
    
    public void editMethod(MethodObj oldMeth, MethodObj newMeth) {
        selected.editMethod(oldMeth, newMeth);
        
        updateClasses();
        dataChanged(true);
        update();
    }
    
    public void removeMethod(int index) {
        selected.removeMethod(index);
        
        dataChanged(true);
        update();
    }
    
    public void removeMethod(MethodObj meth) {
        selected.removeMethod(meth);
        
        dataChanged(true);
        update();
    }
    
    public void editName(String name) {
        if(selected != null) {
            selected.setName(name);
            
            update();
            dataChanged(true);
        }
    }
    
    public void editPackage(String pack) {
        if(selected != null) {
            selected.setPackage(pack);
            
            dataChanged(true);
        }
    }
    
    public void editParent(String parent) {
        if(selected != null) {
            selected.setParent(parent);
            
            updateClasses();
            update();
            dataChanged(true);
        }
    }
    
    public void editInterfaces(String inter) {
        if(selected != null && !selected.getInterfaces().contains(inter)) {
            selected.addInterface(inter);

            updateClasses();
            update();
            dataChanged(true);
        }
    }
    
    public void setInterfaces(ArrayList<String> interfaces) {
        if(selected != null) {
            selected.setInterfaces(interfaces);

            updateClasses();
            update();
            dataChanged(true);
        }
    }
    
    public void enableSelect() {
        disableStates();
        canSelect = true;
        
        setCursor(CURSOR_SELECT);
    }
    
    public void enableResize() {
        disableStates();
        canResize = true;
        
        setCursor(CURSOR_DEFAULT);
    }
    
    public void enableDelete() {
        disableStates();
        canDelete = true;
        
        setCursor(CURSOR_DELETE);
    }
    
    private void disableStates() {
        canSelect = false;
        canResize = false;
        canDelete = false;
        
        moving = false;
        resizing = false;
        panning = false;
    }
    
    public void setGridRender(boolean render) {
        this.gridRender = render;
        
        grid.setVisible(render);
        
        update();
    }
    
    public void setGridSnap(boolean snap) {
        this.gridSnap = snap;
    }
    
    public void zoomIn() {
        paneScaleX *= PANE_SCALE_FACTOR;
        paneScaleY *= PANE_SCALE_FACTOR;
        
        pane.setScaleX(paneScaleX);
        pane.setScaleY(paneScaleY);
    }
    
    public void zoomOut() {
        paneScaleX /= PANE_SCALE_FACTOR;
        paneScaleY /= PANE_SCALE_FACTOR;
        
        pane.setScaleX(paneScaleX);
        pane.setScaleY(paneScaleY);
    }
    
    private int searchUnderMouse() {
        for(int i = classes.size() - 1; i >= 0; i--) {
            ClassObj obj = classes.get(i);

            if(mouseX > obj.getX() && mouseY > obj.getY()
                    && mouseX < obj.getX() + obj.getWidth()
                    && mouseY < obj.getY() + obj.getHeight()) {

                return i;
            }
        }
        
        return -1;
    }
    
    public void mousePressed() {
        int classIndex = searchUnderMouse();
        
        if(classIndex == -1) {                                                      // PANNING
            
            selected = null;
            selectedIndex = -1;
            
            panning = true;

            xOffset = mouseX;
            yOffset = mouseY;

            prevHvalue = scrollPane.getHvalue();
            prevVvalue = scrollPane.getVvalue();

            setCursor(CURSOR_PAN);
            
        } else if(canSelect) {                                                      // SELECTING
            
            if(classIndex != -1) {
                select(classIndex);
            
                setCursor(CURSOR_DRAG);
            }
            
        } else if(canDelete) {                                                      // REMOVING
            
            if(classIndex != -1) {
                remove(classIndex);
            }
            
            setCursor(CURSOR_DELETE);
        } else if(canResize) {                                                      // RESIZING [incomplete]
            
        } else {
            resetCursor();
        }
        
        // UPDATE TEXT FIELDS
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.updateComponents(selected);
        
        // Make workspace and controls aware of the changes
        dataChanged(true);
        update();
    }
    
    public void mouseReleased() {
        if(moving) {
            moving = false;
            
            setCursor(CURSOR_SELECT);
        } else if(panning) {
            panning = false;
        }
        
        resetCursor();
        dataChanged(true);
    }
    
    public void mouseDragged() {
        if(canSelect && moving && selected != null) {
            selected.setX(mouseX - xOffset);
            selected.setY(mouseY - yOffset);
            
            for(ConnectorObj conn : selected.getConnections()) {
                conn.moveNode(conn.getArrow(), mouseX - xOffset, mouseY - yOffset);
            }
            
            moveConnectors();
            
            if(gridSnap) {
                selected.setX(selected.getX() - (selected.getX() % gridCellWidth));
                selected.setY(selected.getY() - (selected.getY() % gridCellHeight));
            }
        } else if(panning) {
            
            if(!gridRender) {
                scrollPane.setHvalue(prevHvalue + ((xOffset - mouseX) / PANE_WIDTH));
                scrollPane.setVvalue(prevVvalue + ((yOffset - mouseY) / PANE_HEIGHT));
            }
        }
        
        dataChanged(true);
    }
    
    private void moveConnectors() {
        for(ClassObj classObj : classes) {
            if(classObj.getName().equals(selected.getName())) {
                continue;
            }

            for(ConnectorObj conn : classObj.getConnections()) {
                if(conn.getDestination().equals(selected.getName())) {
                    conn.moveNode(conn.getOrigin(), selected.getX(), selected.getY());
                }
            }
        }
    }
    
    private void setCursor(Cursor cursor) {
        scrollPane.setCursor(cursor);
        pane.setCursor(cursor);
    }
    
    private void resetCursor() {
        if(canSelect) {
            setCursor(CURSOR_SELECT);
        } else if(canDelete) {
            setCursor(CURSOR_DELETE);
        }
    }
    
    public void loadNewData(ArrayList<ClassObj> classes) {
        this.classes = classes;
        
        selected = null;
        selectedIndex = -1;
        
        update();
        dataChanged(false);
    }
    
    public void printData() {
        for(int i = 0; i < classes.size(); i++) {
            System.out.println(classes.get(i) + "\n");
        }
    }
    
    public void dataChanged(boolean changed) {
        if(app != null) {
            app.getGUI().updateToolbarControls(!changed);
        }
    }
    
    public WritableImage toPhoto() {
        return pane.snapshot(new SnapshotParameters(), null);
    }

    /**
     * This function clears out the data in the Workspace and updates it visually
     */
    @Override
    public void reset() {
        classes = new ArrayList();
        selected = null;
        selectedIndex = -1;
        update();
    }
}