package jcd.test_bed;

import java.io.IOException;
import jcd.data.DataManager;
import jcd.file.FileManager;
import saf.components.AppDataComponent;

/**
 *
 * @author Nathan
 */
public class TestLoad {
    public static void main(String[] args) {
        DataManager dataManager = new DataManager();
        testLoad(dataManager);
    }
    
    public static void testLoad(AppDataComponent data) {
        DataManager dataManager = (DataManager)data;
        
        // FILEPATHS TO RETRIEVE
        String filename = "DesignSaveTest.json";
        String filepath = "./work/";
        
        System.out.println("Attempting to load classes from '" + filepath + filename + "'.");
        FileManager fileManager = new FileManager();

        long startTime = System.currentTimeMillis();
        
        try {
            fileManager.loadData(dataManager, filepath + filename);
            System.out.println("Loading was successful!");
        } catch(IOException e) {
            System.out.println("Failed to load '" + filename + filepath +"'.");
        }
        
        long endTime = System.currentTimeMillis();
        System.out.println("Time Ellapsed: " + (endTime - startTime) + " ms");
        
        // PRINT OUT THE CLASSES THAT HAVE BEEN LOADED
        System.out.println("\nLOADED TEST CLASSES");
        System.out.println("------------------------------------------------------------------------------");
        dataManager.printData();
        System.out.println("------------------------------------------------------------------------------");
    }
}
