package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import jcd.data.DataManager;
import jcd.file.FileManager;
import static jcd.test_bed.TestSave.buildClasses;
import saf.components.AppDataComponent;
import jcd.objects.ClassObj;

/**
 *
 * @author Nathan
 */
public class TestCode {
    public static void main(String[] args) {
        DataManager dataManager = new DataManager();
        testCode(dataManager);
    }
    
    public static void testCode(AppDataComponent data) {
        DataManager dataManager = (DataManager)data;
        dataManager.setClasses(buildClasses());
        
        FileManager fileManager = new FileManager();
        
        ArrayList<ClassObj> classes = dataManager.getClasses();
        
        classes.add(TestSave.buildApplication());
        classes.add(TestSave.buildEvent());
        
        // FILEPATH FOR TEST FILE
        String filepath = "./test/code/";
        
        // PRINTING OUT THE CODE TO THE CONSOLE
        System.out.println("Converting classes to code...");
        
        for(ClassObj classObj : classes) {
            System.out.println("\n------------------------------------------------------------------------------");
            System.out.println(classObj.toCode());
        }
        
        System.out.println("\n------------------------------------------------------------------------------");
        
        long startTime = System.currentTimeMillis();
        
        // EXPORTING THE CODE TO THE FILEPATH
        try {
            fileManager.exportData(dataManager, filepath);
            System.out.println("\nSuccessfully exported to '" + filepath + "'.");
        } catch(IOException e) {
            System.out.println("\nFAILED TO EXPORT TO '" + filepath + "'.");
        }
        
        long endTime = System.currentTimeMillis();
        
        System.out.println("Time Ellapsed: " + (endTime - startTime) + " ms");
    }
}
