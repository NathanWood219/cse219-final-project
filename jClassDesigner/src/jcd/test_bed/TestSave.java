package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import jcd.data.DataManager;
import jcd.file.FileManager;
import jcd.objects.Arrow;
import jcd.objects.ClassObj;
import jcd.objects.ConnectorObj;
import jcd.objects.MethodObj;
import jcd.objects.Type;
import jcd.objects.VariableObj;
import saf.components.AppDataComponent;

/**
 *
 * @author Nathan
 */
public class TestSave {
    public static void main(String[] args) {
        DataManager dataManager = new DataManager();
        testSave(dataManager);
    }
    
    public static void testSave(AppDataComponent data) {
        DataManager dataManager = (DataManager)data;
        
        System.out.println("Creating classes...");
        
        // ADD TO DATA MANAGER
        
        ArrayList<ClassObj> classes = new ArrayList();
        classes.add(buildClass());
        dataManager.setClasses(classes);
        
        //dataManager.setClasses(buildClasses());
        
        // PRINT OUT THE CLASSES THAT HAVE BEEN CREATED
        System.out.println("CREATED TEST CLASSES");
        System.out.println("------------------------------------------------------------------------------");
        dataManager.printData();
        System.out.println("------------------------------------------------------------------------------");
        
        // SEND TO FILE MANAGER FOR SAVING TO JSON FORMAT
        String filename = "DesignSaveTest.json";
        String filepath = "./work/";
        
        System.out.println("Attempting to save classes to '" + filepath + filename + "'.");
        FileManager fileManager = new FileManager();
        
        long startTime = System.currentTimeMillis();
        
        try {
            fileManager.saveData(dataManager, filepath + filename);
            System.out.println("Saving was successful!");
        } catch(IOException e) {
            System.out.println("Failed to save as '" + filename + filepath +"'.");
        }
        
        long endTime = System.currentTimeMillis();
        
        System.out.println("Time Ellapsed: " + (endTime - startTime) + " ms");
    }
    
    public static ClassObj buildClass() {
        ClassObj lineNode = new ClassObj("LineNode", Type.CLASS);
        
        lineNode.addVariable(new VariableObj("x", "double", "private"));
        lineNode.addVariable(new VariableObj("y", "double", "private"));
        
        lineNode.addMethod(new MethodObj("toJSON", "JsonObject", "public"));
        
        return lineNode;
    }
    
    public static ArrayList<ClassObj> buildClasses() {
        ArrayList<ClassObj> classes = new ArrayList();
        
        // HARD CODED CUSTOM CLASSES USING THREADEXAMPLE
        ClassObj thread = new ClassObj("ThreadExample", Type.CLASS);
        
        thread.addVariable(new VariableObj("START_TEXT",       "String",       "public",  true));
        thread.addVariable(new VariableObj("PAUSE_TEXT",       "String",       "public",  true));
        thread.addVariable(new VariableObj("window",           "Stage",        "private"));
        thread.addVariable(new VariableObj("appPane",          "BorderPane",   "private"));
        thread.addVariable(new VariableObj("topPane",          "FlowPane",     "private"));
        thread.addVariable(new VariableObj("startButton",      "Button",       "private"));
        thread.addVariable(new VariableObj("pausebutton",      "Button",       "private"));
        thread.addVariable(new VariableObj("scrollPane",       "ScrollPane",   "private"));
        thread.addVariable(new VariableObj("textArea",         "TextArea",     "private"));
        thread.addVariable(new VariableObj("dateThread",       "Thread",       "private"));
        thread.addVariable(new VariableObj("dateTask",         "Task",         "private"));
        thread.addVariable(new VariableObj("counterThread",    "Thread",       "private"));
        thread.addVariable(new VariableObj("counterTask",      "Task",         "private"));
        thread.addVariable(new VariableObj("work",             "boolean",      "private"));
        
        thread.addMethod(new MethodObj("start",        "void",     "public",   false,  false,  new VariableObj("primaryStage", "Stage")));
        thread.addMethod(new MethodObj("startWork",    "void",     "public"));
        thread.addMethod(new MethodObj("pauseWork",    "void",     "public"));
        thread.addMethod(new MethodObj("doWork",       "boolean",  "public"));
        thread.addMethod(new MethodObj("appendText",   "void",     "public",   false,  false,  new VariableObj("textToAppend", "String")));
        thread.addMethod(new MethodObj("sleep",        "void",     "public",   false,  false,  new VariableObj("timeToSleep", "int")));
        thread.addMethod(new MethodObj("initLayout",   "void",     "private"));
        thread.addMethod(new MethodObj("initHandlers", "void",     "private"));
        thread.addMethod(new MethodObj("initWindow",   "void",     "private",  false,  false,  new VariableObj("initPrimaryStage", "Stage")));
        thread.addMethod(new MethodObj("initThreads",  "void",     "private"));
        thread.addMethod(new MethodObj("main",         "void",     "public",   true,   false,  new VariableObj("args", "String[]")));
        
        classes.add(thread);
        
        // HARD CODED COUNTERTASK CLASS
        ClassObj counter = new ClassObj("CounterTask", Type.CLASS);
        
        counter.addVariable(new VariableObj("app",         "ThreadExample",    "private"));
        counter.addVariable(new VariableObj("counter",     "int",              "private"));
        
        counter.addMethod(new MethodObj("CounterTask",     "",         "public",   false,  false,  new VariableObj("initApp", "ThreadExample")));
        counter.addMethod(new MethodObj("call",            "void",     "protected"));
        
        classes.add(counter);
        
        // HARD CODED DATETASK CLASS
        ClassObj date = new ClassObj("DateTask", Type.CLASS);
        
        date.addVariable(new VariableObj("app",        "ThreadExample",    "private"));
        date.addVariable(new VariableObj("now",        "Date",             "private"));
        
        date.addMethod(new MethodObj("DateTask",       "",         "public",   false,  false,  new VariableObj("initApp", "ThreadExample")));
        date.addMethod(new MethodObj("call",           "void",     "protected"));
        
        classes.add(date);
        
        // HARD CODED PAUSEHANDLER CLAS
        ClassObj pause = new ClassObj("PauseHandler", Type.CLASS);
        
        pause.addVariable(new VariableObj("app",       "ThreadExample",   "private"));
        
        pause.addMethod(new MethodObj("PauseHandler",      "",         "public",   false,  false,  new VariableObj("initApp", "ThreadExample")));
        pause.addMethod(new MethodObj("handle",            "void",     "public",   false,  false, new VariableObj("event", "Event")));
        
        classes.add(pause);
        
        // HARD CODED PAUSEHANDLER CLAS
        ClassObj start = new ClassObj("StartHandler", Type.CLASS);
        
        start.addVariable(new VariableObj("app",       "ThreadExample",   "private"));
        
        start.addMethod(new MethodObj("StartHandler",      "",         "public",   false,  false,  new VariableObj("initApp", "ThreadExample")));
        start.addMethod(new MethodObj("handle",            "void",     "public",   false,  false, new VariableObj("event", "Event")));
        
        classes.add(start);
        
        // SETTING CONNECTIONS BETWEEN CLASSES
        thread.addConnection(new ConnectorObj(Arrow.AGGREGATION, counter));
        thread.addConnection(new ConnectorObj(Arrow.AGGREGATION, date));
        thread.addConnection(new ConnectorObj(Arrow.AGGREGATION, pause));
        thread.addConnection(new ConnectorObj(Arrow.AGGREGATION, start));
        
        counter.addConnection(new ConnectorObj(Arrow.AGGREGATION, thread));
        date.addConnection(new ConnectorObj(Arrow.AGGREGATION, thread));
        pause.addConnection(new ConnectorObj(Arrow.AGGREGATION, thread));
        start.addConnection(new ConnectorObj(Arrow.AGGREGATION, thread));
        
        // ADDING API
        ClassObj taskAPI = new ClassObj("Task", Type.CLASS, "javafx.concurrent");
        ClassObj dateAPI = new ClassObj("Date", Type.CLASS, "java.util");
        ClassObj platformAPI = new ClassObj("Platform", Type.CLASS, "javafx.application");
        ClassObj stageAPI = new ClassObj("Stage", Type.CLASS, "javafx.stage");
        ClassObj borderpaneAPI = new ClassObj("BorderPane", Type.CLASS, "javafx.scene.layout");
        ClassObj flowpaneAPI = new ClassObj("FlowPane", Type.CLASS, "javafx.scene.layout");
        ClassObj buttonAPI = new ClassObj("Button", Type.CLASS, "javafx.scene.control");
        ClassObj scrollpaneAPI = new ClassObj("ScrollPane", Type.CLASS, "javafx.scene.control");
        ClassObj textareaAPI = new ClassObj("TextArea", Type.CLASS, "javafx.scene.control");
        ClassObj threadAPI = new ClassObj("Thread", Type.CLASS);
        
        // SETTING CONNECTIONS FOR API
        counter.addConnection(new ConnectorObj(Arrow.GENERALIZATION, taskAPI));
        counter.addConnection(new ConnectorObj(Arrow.INHERITENCE, platformAPI));
        date.addConnection(new ConnectorObj(Arrow.GENERALIZATION, taskAPI));
        date.addConnection(new ConnectorObj(Arrow.INHERITENCE, platformAPI));
        dateAPI.addConnection(new ConnectorObj(Arrow.AGGREGATION, date));
        
        stageAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        borderpaneAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        flowpaneAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        buttonAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        scrollpaneAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        textareaAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        threadAPI.addConnection(new ConnectorObj(Arrow.INHERITENCE, thread));
        
        classes.add(taskAPI);
        classes.add(dateAPI);
        classes.add(platformAPI);
        classes.add(stageAPI);
        classes.add(borderpaneAPI);
        classes.add(flowpaneAPI);
        classes.add(buttonAPI);
        classes.add(scrollpaneAPI);
        classes.add(textareaAPI);
        classes.add(threadAPI);
        
        return classes;
    }
    
    public static ClassObj buildApplication() {
        ClassObj application = new ClassObj("Application", Type.ABSTRACT);
        application.addMethod(new MethodObj("start",   "void", "public", false, false, new VariableObj("primaryStage", "Stage")));
        application.setPackage("javafx.application");
        application.setX(200);
        application.setY(320);
        
        return application;
    }
    
    public static ClassObj buildEvent() {
        ClassObj event = new ClassObj("EventHandler", Type.INTERFACE);
        event.addMethod(new MethodObj("handle",   "void", "public", false, false, new VariableObj("event", "Event")));
        event.setPackage("javafx.event");
        event.setX(160);
        event.setY(380);
        
        return event;
    }
}
