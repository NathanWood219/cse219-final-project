package jcd;

/**
 * These are properties that are to be loaded from workspace_properties.xml. They
 * will provide custom labels and other UI details for this application and
 * it's custom workspace. Note that in this application we're using two different
 * properties XML files. simple_app_properties.xml is for settings known to the
 * Simple App Framework and so helps to set it up. These properties would be for
 * anything relevant to this custom application. The reason for loading this stuff
 * from an XML file like this is to make these settings independent of the code
 * and therefore easily interchangeable, like if we wished to change the language
 * the application ran in.
 * 
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public enum PropertyType {    
    SELECT_EDIT_ICON,
    SELECT_EDIT_TOOLTIP,
    RESIZE_EDIT_ICON,
    RESIZE_EDIT_TOOLTIP,
    ADDCLASS_EDIT_ICON,
    ADDCLASS_EDIT_TOOLTIP,
    ADDINTERFACE_EDIT_ICON,
    ADDINTERFACE_EDIT_TOOLTIP,
    REMOVE_EDIT_ICON,
    REMOVE_EDIT_TOOLTIP,
    UNDO_EDIT_ICON,
    UNDO_EDIT_TOOLTIP,
    REDO_EDIT_ICON,
    REDO_EDIT_TOOLTIP,
    
    ZOOMIN_VIEW_ICON,
    ZOOMIN_VIEW_TOOLTIP,
    ZOOMOUT_VIEW_ICON,
    ZOOMOUT_VIEW_TOOLTIP,
    GRID_VIEW_TEXT,
    GRID_VIEW_TOOLTIP,
    SNAP_VIEW_TEXT,
    SNAP_VIEW_TOOLTIP,
    
    // COMPONENTS
    COMPONENT_ADD_ICON,
    COMPONENT_ADD_TOOLTIP,
    COMPONENT_REMOVE_ICON,
    COMPONENT_REMOVE_TOOLTIP,

    ATTRIBUTE_UPDATE_ERROR_MESSAGE,
    ATTRIBUTE_UPDATE_ERROR_TITLE,
    ADD_ELEMENT_ERROR_MESSAGE,
    ADD_ELEMENT_ERROR_TITLE,
    REMOVE_ELEMENT_ERROR_MESSAGE,
    REMOVE_ELEMENT_ERROR_TITLE,
    ILLEGAL_NODE_REMOVAL_ERROR_MESSAGE, 
    ILLEGAL_NODE_REMOVAL_ERROR_TITLE,
    TEMP_PAGE_LOAD_ERROR_MESSAGE,
    TEMP_PAGE_LOAD_ERROR_TITLE,
    CSS_EXPORT_ERROR_MESSAGE,
    CSS_EXPORT_ERROR_TITLE,
    UPDATE_ERROR_MESSAGE,
    UPDATE_ERROR_TITLE
}