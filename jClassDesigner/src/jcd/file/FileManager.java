package jcd.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import jcd.objects.ClassObj;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    // CONSTANTS FOR JSON OBJECT BUILDING
    private final String JSON_CLASSES_ARRAY = "classes";
    
    // EXPORTING TO CODE
    private final String DEFAULT_PACKAGE = "<default package>";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
        DataManager dataManager = (DataManager)data;
        
        // THEN THE ARRAYS
	JsonArrayBuilder classesJsonBuilder = Json.createArrayBuilder();
        ArrayList<ClassObj> classes = dataManager.getClasses();
        
        for(ClassObj classObj : classes) {
            classesJsonBuilder.add(classObj.toJSON());
        }
        
	JsonArray classesArray = classesJsonBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_CLASSES_ARRAY, classesArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
        
        JsonObject jsonObject = loadJSONFile(filePath);
        JsonArray classesJsonArray = jsonObject.getJsonArray(JSON_CLASSES_ARRAY);
        
        // Load all the classes
        ArrayList<ClassObj> classes = new ArrayList();
        
        for(int i = 0; i < classesJsonArray.size(); i++) {
            classes.add(ClassObj.fromJSON(classesJsonArray.getJsonObject(i)));
        }
        
        // RELOAD WITH THE NEW DATA
        dataManager.loadNewData(classes);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to 
     * Java source code with the proper packages and directories
     * 
     * @param data The data management component.
     * 
     * @param filePath directory to export source code
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the files.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
        
        for(ClassObj classObj : dataManager.getClasses()) {
            if(classObj.isAPI()) {
                continue;
            }
            
            String subFilePath = packageToFilePath(classObj.getPackage());
            
            File dir = new File(filePath + subFilePath);
            System.out.println("Full path: " + filePath + subFilePath);
            
            if(!dir.exists()) {
                dir.mkdirs();
            }
            
            File f = new File(filePath + subFilePath + "/" + classObj.getName() + ".java");
            System.out.println("Filepath: " + filePath + subFilePath + "/" + classObj.getName() + ".java");
            
            FileWriter fw = new FileWriter(f);
            PrintWriter pw = new PrintWriter(fw);

            pw.println(classObj.toCode());

            fw.close();
            pw.close();
        }
    }
        
    private String packageToFilePath(String pack) {
        if(pack.equals(DEFAULT_PACKAGE)) {
            return "";
        }
        
        String filepath = "/";
        
        for(int i = 0; i < pack.length(); i++) {
            if(pack.charAt(i) == '.') {
                filepath += "/";
            } else {
                filepath += pack.charAt(i);
            }
        }
        
        return filepath;
    }
    
    @Override
    public void exportPhoto(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
        
        ImageIO.write(SwingFXUtils.fromFXImage(dataManager.toPhoto(), null), "png", new File(filePath));
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
