
/**
 * Default description for ThreadExample
 * 
 * @author Nathan
 * @version 1.0
 */

public class ThreadExample {
	public static String START_TEXT;
	public static String PAUSE_TEXT;
	private Stage window;
	private BorderPane appPane;
	private FlowPane topPane;
	private Button startButton;
	private Button pausebutton;
	private ScrollPane scrollPane;
	private TextArea textArea;
	private Thread dateThread;
	private Task dateTask;
	private Thread counterThread;
	private Task counterTask;
	private boolean work;

	/**
	 * Default description for start
	 * 
	 * @param primaryStage 
	 * 
	 */
	public void start(Stage primaryStage) {}

	/**
	 * Default description for startWork
	 * 
	 */
	public void startWork() {}

	/**
	 * Default description for pauseWork
	 * 
	 */
	public void pauseWork() {}

	/**
	 * Default description for doWork
	 * 
	 * @return 
	 * 
	 */
	public boolean doWork() { return false; }

	/**
	 * Default description for appendText
	 * 
	 * @param textToAppend 
	 * 
	 */
	public void appendText(String textToAppend) {}

	/**
	 * Default description for sleep
	 * 
	 * @param timeToSleep 
	 * 
	 */
	public void sleep(int timeToSleep) {}

	/**
	 * Default description for initLayout
	 * 
	 */
	private void initLayout() {}

	/**
	 * Default description for initHandlers
	 * 
	 */
	private void initHandlers() {}

	/**
	 * Default description for initWindow
	 * 
	 * @param initPrimaryStage 
	 * 
	 */
	private void initWindow(Stage initPrimaryStage) {}

	/**
	 * Default description for initThreads
	 * 
	 */
	private void initThreads() {}

	/**
	 * Default description for main
	 * 
	 * @param args 
	 * 
	 */
	public static void main(String[] args) {}

}
