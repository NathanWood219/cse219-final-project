package dummy.data;


/**
 * Default description for Dummy
 * 
 * @author Nathan
 * @version 1.0
 */

public class Dummy extends AbstractDummy  {
    private int myVar;
    public static String MY_VAR;

    /**
     * Default description for myMeth
     * 
     * @param arg1 
     * 
     * @param arg2 
     * 
     */
    private void myMeth(int arg1, Dummy arg2) {}

    /**
     * Default description for init
     * 
     * @return 
     * 
     */
    public static String init() { return ""; }

    /**
     * Default description for update
     * 
     */
    public void update() {}

}
