import java.io.IOException;
import jcd.data.DataManager;
import jcd.file.FileManager;
import jcd.test_bed.TestSave;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import jcd.objects.ClassObj;

/**
 * Compares the default structure of the ThreadExample UML classes diagram
 * that is constructed in jcd.test_bed.TestSave.java (hard-coded construction)
 * 
 * @author Nathan
 */
public class JUnitTest1 {
    private final String TEST_FILEPATH = "./test/saves/JUnitTest1.json";
    
    public JUnitTest1() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void testFileIO() {
        System.out.println("* JUnitTest1: test methods - FileManager.saveData(), FileManager.loadData()\n\tUsing default version of ThreadExample");
        
        FileManager fileManager = new FileManager();
        DataManager saveDM = new DataManager();
        DataManager loadDM = new DataManager();
        
        saveDM.setClasses(TestSave.buildClasses());
        
        try {
            fileManager.saveData(saveDM, TEST_FILEPATH);
            fileManager.loadData(loadDM, TEST_FILEPATH);
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        // MAKING COMPARISONS OF ALL DATA
        assertEquals(saveDM.getClasses().size(),    loadDM.getClasses().size());
        
        for(int i = 0; i < saveDM.getClasses().size(); i++) {
            assertEquals(saveDM.getClasses().get(i),    loadDM.getClasses().get(i));
        }
        
        // MAKING COMPARISONS
        /*
        assertEquals(saveDM.getClasses().get(0).getVariables(), loadDM.getClasses().get(0).getVariables());
        assertEquals(saveDM.getClasses().get(1).getVariables(), loadDM.getClasses().get(1).getVariables());
        assertEquals(saveDM.getClasses().get(2).getVariables(), loadDM.getClasses().get(2).getVariables());
        assertEquals(saveDM.getClasses().get(3).getVariables(), loadDM.getClasses().get(3).getVariables());
        
        assertEquals(saveDM.getClasses().get(0).getMethods(), loadDM.getClasses().get(0).getMethods());
        assertEquals(saveDM.getClasses().get(1).getMethods(), loadDM.getClasses().get(1).getMethods());
        assertEquals(saveDM.getClasses().get(2).getMethods(), loadDM.getClasses().get(2).getMethods());
        assertEquals(saveDM.getClasses().get(3).getMethods(), loadDM.getClasses().get(3).getMethods());
        
        assertEquals((int)saveDM.getClasses().get(0).getX(),    (int)loadDM.getClasses().get(0).getX());
        assertEquals((int)saveDM.getClasses().get(0).getY(),    (int)loadDM.getClasses().get(0).getY());
        assertEquals(saveDM.getClasses().get(1).getName(),      loadDM.getClasses().get(1).getName());
        assertEquals(saveDM.getClasses().get(2).getPackage(),   loadDM.getClasses().get(2).getPackage());
        */
    }
}
