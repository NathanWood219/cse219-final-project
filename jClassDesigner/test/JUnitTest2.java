import java.io.IOException;
import java.util.ArrayList;
import jcd.data.DataManager;
import jcd.file.FileManager;
import jcd.objects.Arrow;
import jcd.test_bed.TestSave;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import jcd.objects.ClassObj;
import jcd.test_bed.TestSave;
import static org.junit.Assert.*;

/**
 * Compares the altered structure of the ThreadExample UML classes diagram
 * that is constructed in jcd.test_bed.TestSave.java (hard-coded construction)
 * 
 *  This tests with a created Abstract class
 * 
 * @author Nathan
 */
public class JUnitTest2 {
    private final String TEST_FILEPATH = "./test/saves/JUnitTest2.json";
    
    public JUnitTest2() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testFileIO() {
        System.out.println("* JUnitTest2: test methods - FileManager.saveData(), FileManager.loadData()\n\tUsing altered version of ThreadExample");
        
        FileManager fileManager = new FileManager();
        DataManager saveDM = new DataManager();
        DataManager loadDM = new DataManager();
        
        ArrayList<ClassObj> classes = TestSave.buildClasses();
        
        classes.add(TestSave.buildApplication());
        
        saveDM.setClasses(classes);
        
        try {
            fileManager.saveData(saveDM, TEST_FILEPATH);
            fileManager.loadData(loadDM, TEST_FILEPATH);
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        // MAKING COMPARISONS OF ALL DATA
        assertEquals(saveDM.getClasses().size(),    loadDM.getClasses().size());
        
        for(int i = 0; i < saveDM.getClasses().size(); i++) {
            assertEquals(saveDM.getClasses().get(i),    loadDM.getClasses().get(i));
        }
        
        // MAKING COMPARISONS
        /*
        assertEquals(saveDM.getClasses().get(4).getType(),          loadDM.getClasses().get(4).getType());
        assertEquals(saveDM.getClasses().get(4).getVariables(),     loadDM.getClasses().get(4).getVariables());
        assertEquals(saveDM.getClasses().get(4).getMethods(),       loadDM.getClasses().get(4).getMethods());
        assertEquals((int)saveDM.getClasses().get(4).getX(),        (int)loadDM.getClasses().get(4).getX());
        assertEquals((int)saveDM.getClasses().get(4).getY(),        (int)loadDM.getClasses().get(4).getY());
        assertEquals((int)saveDM.getClasses().get(4).getWidth(),    (int)loadDM.getClasses().get(4).getWidth());
        assertEquals((int)saveDM.getClasses().get(4).getHeight(),   (int)loadDM.getClasses().get(4).getHeight());
        assertEquals(saveDM.getClasses().get(4).getName(),          loadDM.getClasses().get(4).getName());
        assertEquals(saveDM.getClasses().get(4).getPackage(),       loadDM.getClasses().get(4).getPackage());
        assertEquals(saveDM.getClasses().get(4).getConnections(),   loadDM.getClasses().get(4).getConnections());
        assertEquals(saveDM.getClasses().get(4).getType(),          loadDM.getClasses().get(4).getType());
        */
    }
}
