import java.io.IOException;
import java.util.ArrayList;
import jcd.data.DataManager;
import jcd.file.FileManager;
import jcd.test_bed.TestSave;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import jcd.objects.ClassObj;
import static org.junit.Assert.*;

/**
 * Compares another altered structure of the ThreadExample UML classes diagram
 * that is constructed in jcd.test_bed.TestSave.java (hard-coded construction)
 * 
 *  This tests with a created Interface
 * 
 * @author Nathan
 */
public class JUnitTest3 {
    private final String TEST_FILEPATH = "./test/saves/JUnitTest3.json";
    
    public JUnitTest3() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testFileIO() {
        System.out.println("* JUnitTest3: test methods - FileManager.saveData(), FileManager.loadData()\n\tUsing second altered version of ThreadExample");
        
        FileManager fileManager = new FileManager();
        DataManager saveDM = new DataManager();
        DataManager loadDM = new DataManager();
        
        ArrayList<ClassObj> classes = TestSave.buildClasses();
        
        classes.add(TestSave.buildApplication());
        classes.add(TestSave.buildEvent());
        
        saveDM.setClasses(classes);
        
        try {
            fileManager.saveData(saveDM, TEST_FILEPATH);
            fileManager.loadData(loadDM, TEST_FILEPATH);
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        // MAKING COMPARISONS OF ALL DATA
        assertEquals(saveDM.getClasses().size(),    loadDM.getClasses().size());
        
        for(int i = 0; i < saveDM.getClasses().size(); i++) {
            assertEquals(saveDM.getClasses().get(i),    loadDM.getClasses().get(i));
        }
    }
}
