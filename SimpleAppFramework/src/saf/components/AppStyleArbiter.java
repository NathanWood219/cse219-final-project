package saf.components;

/**
 * This interface serves as a family of type that will initialize
 * the style for some set of controls, like the workspace, for example.
 * 
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public interface AppStyleArbiter {
    // THESE ARE COMMON STYLE CLASSES WE'LL USE
    public static final String CLASS_BORDERED_PANE = "bordered_pane";
    public static final String CLASS_WORKSPACE_AREA = "workspace_area";
    public static final String CLASS_COMPONENTS_AREA = "components_area";
    public static final String CLASS_VBOX_OBJECT = "vbox_object";
    public static final String CLASS_VBOX_HEADER = "vbox_header";
    public static final String CLASS_VBOX_LABEL = "vbox_label";
    public static final String CLASS_SELECTED_OBJECT = "selected_object";
    public static final String CLASS_HEADING_LABEL = "heading_label";
    public static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    public static final String CLASS_PROMPT_LABEL = "prompt_label";
    public static final String CLASS_PROMPT_TEXT_FIELD = "prompt_text_field";
    public static final String CLASS_FILE_BUTTON = "file_button";
    
    public void initStyle();
}
